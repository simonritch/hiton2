@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li class="active">Categories</li>
    </ol>
        <div class="row">
          <div class="col-lg-12 col-sm-12">
          @if($errors->first('label'))
            <div class="alert alert-danger" role="alert">{{ $errors->first('label') }}</div>
          @endif
          @if(session()->has('message'))
              <div class="alert alert-success">
             {{ session()->get('message') }}
             </div>
          @endif
            <div class="panel panel-default">
                <div class="panel-body">
                      {{ Form::model(new \App\Category(), array('route' => 'category-do-create')) }}

                      <div class="form-group{{ $errors->has('label') ? ' has-error' : '' }}">

                           {{ Form::label('label', 'label') }}
                              {{ Form::text('label', old("name"),array("class" => "form-control")) }}
                              @if ($errors->has('label'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('label') }}</strong>
                                  </span>
                              @endif
                      </div>

                      {{ Form::submit('Create', array("class" => "btn btn-primary")) }}

                      {{ Form::close() }}
                </div>
              </div>
          </div>

    </div>
    <div class="row">
      @foreach ($categories as $category)
        <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                  {{$category->label}}
                  @if(Entrust::hasRole('admin'))
                  <div style="position: relative; float:right;"><a href="{{route('category-delete',["id" => $category->id])}}"><span class="glyphicon glyphicon-remove-circle " aria-hidden="true"></span></a></div>
                  @endif
              </div>
            </div>
        </div>

        @endforeach
    </div>
</div>
@endsection
