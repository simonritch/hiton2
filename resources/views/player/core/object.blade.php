// Right hand side hit list item
/**
 * @constructor
 */
HitListItem = function (title, url, image, auto)
{
    this.type = "HitListItem";
    this.title = title;
    this.url = url;
    this.image = image;
    this.auto = auto || false;
};

// Right hand side hit list item which is a subvideo
/**
 * @constructor
 */
HitListSubvideoItem = function (title, subvideoId, image, auto)
{
    this.type = "HitListSubvideoItem";
    this.title = title;
    this.subvideoId = subvideoId;
    this.image = image;
    this.auto = auto || false;
};

// Element only visible in the HitList Summary
/**
 * @constructor
 */
HitListSummaryItem = function (title, url, image, auto)
{
    this.type = "HitListSummaryItem";
    this.title = title;
    this.url = url;
    this.image = image;
    this.auto = auto || false;
};

// Bottom product description item
/**
 * @constructor
 */
ProductDescriptionItem = function (title, url, desc, image, auto)
{
    this.type = "ProductDescriptionItem";
    this.title = title;
    this.url = url;
    this.description = desc;
    this.image = image;
    this.auto = auto || false;
};

// Bottom item to show a subvideo
/**
 * @constructor
 */
SubvideoItem = function (title, description, image, branchId, auto)
{
    this.type = "SubvideoItem";
    this.title = title;
    this.image = image;
    this.description = description;
    this.branchId = branchId;
    this.auto = auto || false;
};

// Bottom item to show a subvideo
/**
 * @constructor
 */
SubvideoHotspot = function (branchId, returnId, auto)
{
    this.type = "SubvideoHotspot";
    this.branchId = branchId;
    this.returnId = returnId;
    this.auto = auto || false;
};

// Bottom item to show a list of hit list items (eg. Justice Crew style)
/**
 * @constructor
 */
MultiItem = function (title, url, image, items, auto)
{
    this.type = "MultiItem";
    this.title = title;
    this.url = url;
    this.image = image;
    this.items = items;
    this.auto = auto || false;
};

// Item to just open in a new window
/**
 * @constructor
 */
UrlItem = function (title, url, image, auto)
{
    this.type = "UrlItem";
    this.title = title;
    this.url = url;
    this.image = image;
    this.auto = auto || false;
};

/**
 * @constructor
 */
EmailItem = function (email, auto)
{
    this.type = "EmailItem";
    this.email = email;
    this.auto = auto || false;
};

/**
 * @constructor
 */
CSVReader = function (sizes, file)
{
    var self = this;
    self.file = file;
    self.sizes = (!sizes)?{ "Orig_X": 800, "Orig_Y": 448, "Video_X": 800, "Video_Y": 450 }:sizes;
    
    this.parseLineCSV = function(lineCSV) {
         var CSV = [];
         lineCSV = lineCSV.replace(/\s*$/g,"");   
         return lineCSV.split(/,/g);
    };
    
    this.readCSV = function(filepath) {
        var strReturn = "";
        var xmlhttp;
        if (window.XMLHttpRequest) xmlhttp = new XMLHttpRequest();
        else xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4)
            {
                if(xmlhttp.status == 200) strReturn = xmlhttp.responseText;
                else console.log("Error loading coordinate file");
            }
        }
        xmlhttp.open("GET", filepath, false);
        xmlhttp.send();
        return (strReturn==null || strReturn.length == 0)?"":strReturn.split(/\r/g);
    };
};
CSVReader.prototype.loadCSVs = function(){
    var self = this;
    var arr = self.readCSV(self.file);

    var map = {};

    for (var i=0; i < arr.length; i++) 
    {
        arr[i] = self.parseLineCSV(arr[i]);
    
        var frameID = arr[i][0];
        var points = [];
        if (arr[i][2] !='' && arr[i][5] != '')
            points.push([arr[i][2] * (self.sizes["Video_X"]/self.sizes["Orig_X"]), arr[i][5] * (self.sizes["Video_Y"]/self.sizes["Orig_Y"])]);
        
        if (arr[i][4] !='' && arr[i][3] != '')
            points.push([arr[i][4] * (self.sizes["Video_X"]/self.sizes["Orig_X"]), arr[i][3] * (self.sizes["Video_Y"]/self.sizes["Orig_Y"])]);
        
        var object = { "ID": arr[i][1], "Points": points };
        
        if (map[frameID] != null) map[frameID].push(object);
        else map[frameID] = [object];
    }

    return map;
};
CSVReader.prototype['loadCSVs'] = CSVReader.prototype.loadCSVs;