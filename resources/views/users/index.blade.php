@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="/home">Home</a></li>
        <li class="active">Users</li>
    </ol>
    <div class="row">
      @foreach ($users as $user)
        <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                  <a href="{{route('user-view',["id" => $user->id])}}">{{$user->name}}</a>
              </div>
                <div class="panel-body">
                      <span class="glyphicon glyphicon-envelope" style="margin-right:10px"></span>Email : {{$user->email}} <br/>
                      <span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Account : {!! $user->account !!}<br/>
                      <span class="glyphicon glyphicon-off" style="margin-right:10px"></span>Status : <a href="{{route('user-status',["id" => $user->id])}}">{!! $user->status !!}</a><br/>
                </div>
            </div>
        </div>

        @endforeach
    </div>
</div>
@endsection
