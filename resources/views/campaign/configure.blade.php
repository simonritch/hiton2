@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-lg-12">
                  <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('campaign-create') }}">Campaign</a></li>
                    <li><a href="#">Configure - Tags / Events</a></li>
                    <li class="active">#{{ $campaign->id }}</li>
                  </ol>
          </div>
          <div class="col-lg-12">

                <ul class="nav nav-pills nav-justified">
                      <li role="presentation" class="active"><a href="{{route('campaign-config',["id" => $campaign->id])}}">Tags / Events</a></li>
                      <li role="presentation"><a href="{{route('campaign-config-video',["id" => $campaign->id])}}">Videos and social links</a></li>
                      <li role="presentation"><a href="{{route('campaign-config-css',["id" => $campaign->id])}}">Style Sheets (CSS)</a></li>
                      <li role="presentation"><a href="{{route('campaign-config-asset',["id" => $campaign->id])}}">Images / Assets</a></li>
                </ul>
                <br/>

                @if($errors->first('structure'))
                  <div class="alert alert-danger" role="alert">{{ $errors->first('structure') }}</div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                   {{ session()->get('message') }}
                   </div>
                @endif

                  {{ Form::model($campaign, array('route' => 'campaign-config-save')) }}
                  {{ Form::submit('Save', array("class" => "btn btn-primary")) }}
                  <br/><br/>
                    <textarea  id="structure" name="structure" cols=150 rows=30>{{ $structure }}</textarea>    
                    {{ Form::hidden('camp_id', $campaign->id) }}        
                  {{ Form::close() }}
          </div>
    </div>

</div>
@endsection
