@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('campaign-create') }}">Campaign</a></li>
        <li class="active">create</li>
    </ol>
    <div class="row">
          <div class="col-lg-8 col-sm-8 col-lg-offset-2">
            <div class="panel panel-default">
                <div class="panel-body">
                      {{ Form::model(new \App\Campaign(), array('route' => 'campaign-do-create')) }}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                           {{ Form::label('name', 'Name') }}
                              {{ Form::text('name', old("name"),array("class" => "form-control")) }}
                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                      </div>

                      <div class="form-group{{ $errors->has('cgid') ? ' has-error' : '' }}">
                      {{ Form::label('cgid', 'Google View ID') }}
                              {{ Form::text('cgid', old("cgid"), array("class" => "form-control")) }}
                              @if ($errors->has('cgid'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('cgid') }}</strong>
                                  </span>
                              @endif
                      </div>

                      <div class="form-group{{ $errors->has('tracking_id') ? ' has-error' : '' }}">
                      {{ Form::label('tracking_id', 'Google Tracking ID') }}
                              {{ Form::text('tracking_id', old("tracking_id"), array("class" => "form-control")) }}
                              @if ($errors->has('tracking_id'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('tracking_id') }}</strong>
                                  </span>
                              @endif
                      </div>

                      <div class="form-group{{ $errors->has('cgid') ? ' has-error' : '' }}">
                      {{ Form::label('category_id', 'Category') }}
                              {{ Form::select('category_id', array_pluck(\App\Category::all(),'label','id'),old("category"), array("class" => "form-control")) }}
                              @if ($errors->has('category_id'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('category_id') }}</strong>
                                  </span>
                              @endif
                      </div>

                      {{ Form::submit('Create', array("class" => "btn btn-default")) }}

                      {{ Form::close() }}
                </div>
              </div>
          </div>

    </div>
</div>
@endsection
