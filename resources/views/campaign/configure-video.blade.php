@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-lg-12">
                  <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('campaign-create') }}">Campaign</a></li>
                    <li><a href="#">Configure - Videos</a></li>
                    <li class="active">#{{ $campaign->id }}</li>
                  </ol>
          </div>
          <div class="col-lg-12">

                <ul class="nav nav-pills nav-justified">
                      <li role="presentation"><a href="{{route('campaign-config',["id" => $campaign->id])}}">Tags / Events</a></li>
                      <li role="presentation" class="active"><a href="#">Videos and social links</a></li>
                      <li role="presentation"><a href="{{route('campaign-config-css',["id" => $campaign->id])}}">Style Sheets (CSS)</a></li>
                     <li role="presentation"><a href="{{route('campaign-config-asset',["id" => $campaign->id])}}">Images / Assets</a></li>
                </ul>
                <br/>

                @if($errors->first('video_elt'))
                  <div class="alert alert-danger" role="alert">{{ $errors->first('video_elt') }}</div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                   {{ session()->get('message') }}
                   </div>
                @endif
            </div>
            <div class="col-lg-12">

                  <div class="panel panel-default">
                      <div class="panel-heading"><h3 class="panel-title">Videos</h3></div>
                      <div class="panel-body">

                              {{ Form::model($campaign, array('route' => 'campaign-config-video-save')) }}
                              {{ Form::hidden('camp_id', $campaign->id) }}
                              <div class="col-lg-10">
                                  <div class="input-group">
                                      <div class="input-group-addon"><span class="glyphicon glyphicon-film" aria-hidden="true"></span></div>
                                        {{ Form::text('video_elt', old("video_elt"), array("class" => "form-control")) }}
                                      <div class="input-group-addon">.m4v / .webm / .ogv</div>
                                  </div>
                              </div>
                              <div class="col-lg-2">
                              {{ Form::submit('Add a video', array("class" => "btn btn-primary")) }}
                              </div>
                              <br/>
                              
                              
                              {{ Form::close() }}
                                <br/>
                                  <div class="col-lg-12" style="padding-top: 20px">
                                      <ul class="list-group">
                                      @foreach ($campaign->videos as $key => $video)                
                                                      <li class="list-group-item">
                                                      <span class="glyphicon glyphicon-film" aria-hidden="true"></span>
                                                      {{($key == 0)?"Main Video":"Sub-video #$key"}}
                                                      &#9679; <b>Url</b> : {{$video->url}}
                                                            {{ Form::model($video, array('method' => 'post', 'route' => array('campaign-config-video-remove', $video->id))) }}
                                                            <button type="submit" class="btn btn-danger pull-right btn-xs" aria-label="Remove" style="position:relative;top:-22px">
                                                                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                            </button>
                                                            {{ Form::close() }}
                                                      </li>
                                      @endforeach
                                      </ul>
                               </div>
                           </div>
                    </div>
                </div>
                 <div class="col-lg-12">

                  <div class="panel panel-default">
                      <div class="panel-heading"><h3 class="panel-title">Social links and additional configuration</h3></div>
                      <div class="panel-body">
                                  {{ Form::model($campaign, array('route' => 'campaign-config-add-save')) }}
                                  {{ Form::hidden('camp_id', $campaign->id) }}
                                  <div class="input-group">
                                      <div class="input-group-addon">Facebook</div>
                                        {{ Form::text('facebook',  isset($config['facebook'])?$config['facebook']:"" , array("class" => "form-control")) }}
                                  </div>
                                  <br/>
                                  <div class="input-group">
                                      <div class="input-group-addon">Instagram</div>
                                        {{ Form::text('instagram', isset($config['instagram'])?$config['instagram']:"", array("class" => "form-control")) }}
                                  </div>
                                  <br/>
                                  <div class="input-group">
                                      <div class="input-group-addon">Twitter</div>
                                        {{ Form::text('twitter', isset($config['twitter'])?$config['twitter']:"", array("class" => "form-control")) }}
                                  </div>
                                  <br/>
                                  <div class="col-lg-4">
                                      <div class="input-group">
                                            {{ Form::label('HitListItemThumbs', "HitListItemThumbs") }}
                                            {{ Form::select('HitListItemThumbs', [false => "No",true => "Yes"], isset($config['HitListItemThumbs'])?$config['HitListItemThumbs']:0 , array("class" => "form-control")) }}
                                      </div>
                                  </div>
                                  <div class="col-lg-4">
                                      <div class="input-group">
                                            {{ Form::label('showProductDescriptionThumbs', "showProductDescriptionThumbs") }}
                                            {{ Form::select('showProductDescriptionThumbs', [false => "No",true => "Yes"], isset($config['showProductDescriptionThumbs'])?$config['showProductDescriptionThumbs']:0, array("class" => "form-control")) }}
                                      </div>
                                  </div>
                                  <div class="col-lg-4">
                                      <div class="input-group">
                                            {{ Form::label('showHitListSummaryItemOnly', "showHitListSummaryItemOnly") }}
                                            {{ Form::select('showHitListSummaryItemOnly', [false => "No",true => "Yes"], isset($config['showHitListSummaryItemOnly'])?$config['showHitListSummaryItemOnly']:0, array("class" => "form-control")) }}
                                      </div>
                                  </div>

                                  <div class="col-lg-4">
                                      <br/>
                                      {{ Form::submit('Save configuration', array("class" => "btn btn-primary")) }}
                                  </div>
                                  {{ Form::close() }}
                      </div>
                  </div>
                </div>
          </div>
    </div>

</div>
@endsection
