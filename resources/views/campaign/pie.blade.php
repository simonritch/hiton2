<div id="pide"></div>

<script2>
$(function () {
    var Highcharts = require('highcharts');

      Highcharts.chart('pie', {
                        chart: {
                            type: 'pie',
                            backgroundColor: 'rgba(255, 255, 255, 0)',
                            style: {color: '#fff'}
                        },
                        colors: ['#FF4600', '#ffffff'],
                        title: {
                            text: 'VISITS',
                            style: {
                                color: '#fff'
                            }
                        },
                        series: [{
                                type: 'pie',
                                name: 'Amount',
                                innerSize: '60%',
                                data: [
                                    ['NEW', 888],
                                    ['RETURNING', 4334],
                                ]
                            }],
                        plotOptions: {
                            pie: {
                                allowPointSelect: true,
                                cursor: 'pointer',
                                dataLabels: {
                                    enabled: true,
                                    style: {
                                        color: '#fff'
                                    }
                                }
                            }
                        },
                        credits: {
                            enabled: false
                        }
                    });
});

</script2>
