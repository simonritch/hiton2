<?php

namespace App\Http\Controllers;

use App\User;
use App\Campaign;
use App\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::all();
        return view('users.index',compact("users"));
    }

    public function view($id)
    {
        $user = User::find($id);
        return view('users.view',compact("user"));
    }

    public function status($id)
    {
        $user = User::find($id);
        $user_role = Role::where("name","=","user")->first();

        if($user->confirmed) {
           $user->detachRole($user_role);
           $user->confirmed = 0;
        } else {
           $role = Role::where("name","=","user");
           $user->attachRole($user_role);
           $user->confirmed = 1;
        }

        $user->save();

        return redirect()->route("user-index");

    }

    public function grant(Request $request, $id)
    {
            $cid = $request->input('campaign');

            $user = User::find($id);

            $user->campaigns()->syncWithoutDetaching([$cid]);


            return redirect('user/'.$id);
    }

    public function deny(Request $request, $id)
    {
            $cid = $request->input('campaign');

            $user = User::find($id);

            $user->campaigns()->detach([$cid]);


            return redirect('user/'.$id);
    }

    public function show(Category $category)
    {
        //
    }

    public function destroy(Category $category)
    {
        //
    }
}
