@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="/">Home</a></li>
        <li class="active">Campaigns</li>
    </ol>
    <div class="row">
      @foreach ($campaigns as $campaign)
        <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                  <a href="{{route('campaign-view',["id" => $campaign->id])}}">{{$campaign->name}}</a>
                  @if(Entrust::hasRole('admin'))
                  <div style="position: relative; float:right;"><a href="{{route('campaign-delete',["id" => $campaign->id])}}"><span class="glyphicon glyphicon-remove-circle " aria-hidden="true"></span></a></div>
                  @endif
              </div>
                <div class="panel-body">
                      <span class="glyphicon glyphicon-tags" style="margin-right:10px"></span>Category : {{$campaign->category['label']}} <br/>
                      <span class="glyphicon glyphicon-send" style="margin-right:10px"></span>GA View ID : {{$campaign->cgid}} 
                      <br/>
                      <span class="glyphicon glyphicon-th-list" style="margin-right:10px"></span>
                      @if( !is_null($campaign->structure))
                          <span>Structure configured</span>
                      @else
                          <span style="color:#FF0000">Structure not configured</span>
                      @endif
                      <br/>
                      <span class="glyphicon glyphicon-time" style="margin-right:10px"></span>Created at : {{$campaign->created_at }}
                </div>
            </div>
        </div>

        @endforeach
    </div>
</div>
@endsection
