<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['role:admin']], function() {

          Route::get('campaign/create', 'CampaignController@create')->name('campaign-create');
          Route::post('campaign/create', 'CampaignController@doCreate')->name('campaign-do-create');
          Route::get('campaign/edit/{id}', 'CampaignController@edit')->name('campaign-edit');
          Route::post('campaign/edit', 'CampaignController@doEdit')->name('campaign-do-edit');
          Route::get('campaign/delete/{id}', 'CampaignController@delete')->name('campaign-delete');

          Route::get('/users', 'UserController@index')->name('user-index');
          Route::get('/user/{id}', 'UserController@view')->name('user-view');
          Route::get('user/{id}/status', 'UserController@status')->name('user-status');

          Route::post('user/{id}/grant', 'UserController@grant')->name('user-grant');
          Route::post('user/{id}/deny', 'UserController@deny')->name('user-deny');

          Route::get('/categories', 'CategoryController@index')->name('category-index');
          Route::get('category/delete/{id}', 'CategoryController@delete')->name('category-delete');
          Route::post('category/create', 'CategoryController@doCreate')->name('category-do-create');

          Route::get('campaign/configure/{id}', 'CampaignController@configure')->name('campaign-config');
          Route::post('campaign/configure', 'CampaignController@configurePost')->name('campaign-config-save');

          Route::get('campaign/configure/video/{id}', 'CampaignController@configureVideo')->name('campaign-config-video');
          Route::post('campaign/configure/video', 'CampaignController@configureVideoPost')->name('campaign-config-video-save');
          Route::post('campaign/configure/addconfig', 'CampaignController@configureAddPost')->name('campaign-config-add-save');
          Route::post('campaign/configure/video/remove/{id}', 'CampaignController@removeVideo')->name('campaign-config-video-remove');

          Route::get('campaign/configure/css/{id}', 'CampaignController@configureCss')->name('campaign-config-css');
          Route::post('campaign/configure/css', 'CampaignController@configureCssPost')->name('campaign-config-css-save');

          Route::get('campaign/configure/assets/{id}', 'CampaignController@configureAsset')->name('campaign-config-asset');

          Route::post('file/post/{id}/{name}', 'CampaignController@uploadFilePost')->name('campaign-upload-file-post');

});

Route::get('player/scripts/{id}/frame_objects.csv', 'PlayerController@csv')->name('player-csv');
Route::get('player/scripts/{id}/object.HotSpot.js', 'PlayerController@objects')->name('player-objects');
Route::get('player/css/{id}/common.css', 'PlayerController@commonCss')->name('player-common-css');
Route::get('player/css/{id}/custom.css', 'PlayerController@css')->name('player-css');
Route::get('player/core/{id}/encoded.js', 'PlayerController@core')->name('player-core');

Route::get('player/image/{id}/{imgid}', 'PlayerController@image')->name('player-image');

Route::get('player/redirect', 'PlayerController@redirect')->name('player-redirect');

Route::get('player/{name}/{id}/{context?}', 'PlayerController@index')->name('player-index');

Route::get('campaign/reports/{id}/{startdate?}/{enddate?}/{debug?}', 'CampaignController@view')->name('campaign-view');
