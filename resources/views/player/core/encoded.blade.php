@include('player.core.object')

@include('player.core.VideoControls')



@if(\Agent::isMobile())
	@include('player.core.ContentTheoryPlayerMobile')
@else
	@include('player.core.ContentTheoryPlayer')
@endif