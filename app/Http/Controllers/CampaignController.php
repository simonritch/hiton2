<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JavaScript;
use Carbon\Carbon;
use \App\Campaign as Campaign;
use \App\Config as Config;
use \App\Video as Video;
use \App\File as File;

class CampaignController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create() {
          return view('campaign.create');
    }

    public function doCreate(Request $request) {

      $this->validate($request, [
          'name' => 'required|unique:campaigns|max:255',
          'cgid' =>  'required|digits_between:8,10|unique:campaigns',
          'category_id' =>  'required',
      ]);


      $data = $request->except("_token");

      //dd($data);
      $Campaign = new Campaign();
      $Campaign->name = $data['name'];
      $Campaign->cgid = $data['cgid'];
      $Campaign->tracking_id = $data['tracking_id'];
      $Campaign->category_id = $data['category_id'];
      $Campaign->user_id = \Auth::user()->id;


      if($Campaign->save()) {
          return Redirect('home');
      } else {
          return view('campaign.create');
      }
    }

    public function view($id, $startdate = null, $enddate = null, $debug_mode = "off") {

        $campaign = \App\Campaign::findorfail($id);

        $campaigns = \Auth::user()->campaigns;

        $authorised = false;
        foreach ($campaigns as $c) {
           if($c->id === $campaign->id) {
              $authorised = true;
           }
        }

        if(\Auth::user()->isAdmin()) $authorised = true;

        if($authorised === false) abort(403);

        $googleClient = \Google::getClient();
        $analytics = \Google::make('analytics');

        $ids = "ga:" . $campaign->cgid;
        $from = ($startdate == null)?Carbon::yesterday()->subMonth()->toDateString():$startdate;
        $to = ($enddate == null)?Carbon::yesterday()->toDateString():$enddate;

        $visits = $analytics->data_ga->get($ids, $from, $to, "ga:visits,ga:users,ga:pageviews,ga:bouncerate,ga:AvgSessionDuration,ga:percentNewVisits,ga:timeOnPage", array());

        $usertype = $analytics->data_ga->get($ids, $from, $to, "ga:sessions", array("dimensions" => "ga:userType"));

        $newsession = (isset($usertype->rows[0][1]))?(intval($usertype->rows[0][1])):0;
        $returnsession = (isset($usertype->rows[1][1]))?(intval($usertype->rows[1][1])):0;

        ////////////////////////////   linechart   /////////////////////////////////////////////
        $visitschart= $analytics->data_ga->get($ids, $from, $to, "ga:visits", array("dimensions" => "ga:date","sort" => "ga:date"));
        $visitschart_data = array();
        $visitschart_categories = SELF::dateRange($from, $to);
        foreach($visitschart->rows as $elt) {
                $visitschart_data[] = (int) $elt[1];
        }

        ////////////////////////////   CountryMap   /////////////////////////////////////////////
        $country = $analytics->data_ga->get($ids, $from, $to, "ga:visits", array("dimensions" => "ga:country","sort" => "-ga:visits"));
        $Country_data = array();
        if($country->rows) {
            foreach($country->rows as $elt) {
                  if($elt[0] == "(not set)") continue;
                  $Country_data[] = array("name" => $elt[0], "value" => $elt[1], "code" => $this->getCountryCode($elt[0]));
            }
        }

      //  dd(json_encode($Country_data));

        ////////////////////////////   Country Table   ////////////////////////////////////////////
        $countryTable_data = $analytics->data_ga->get($ids, $from, $to, "ga:visits,ga:pageviews,ga:AvgSessionDuration,ga:bouncerate", array("dimensions" => "ga:country","sort" => "-ga:visits"));

        ////////////////////////////   PieBrowser   ////////////////////////////////////////////
        $Browsers_input = $analytics->data_ga->get($ids, $from, $to, "ga:sessions", array("dimensions" => "ga:browser"));
        $Browsers_data = array();
        if($Browsers_input->rows) {
              foreach ($Browsers_input->rows as $elt) {
                $Browsers_data[] = array( 0 => $elt[0], 1 => floatval($elt[1]));
              }
        }

        ////////////////////////////   PieDevice   ////////////////////////////////////////////
        $Devices_input = $analytics->data_ga->get($ids, $from, $to, "ga:sessions", array("dimensions" => "ga:mobileDeviceInfo"));
        $Devices_data = array();
        if($Devices_input->rows) {
              foreach ($Devices_input->rows as $elt) {
                $Devices_data[] = array( 0 => $elt[0], 1 => floatval($elt[1]));
              }
        }

        ///////////////////////////// Badges   /////////////////////////////////////////////////
        $visits = $analytics->data_ga->get($ids, $from, $to, "ga:visits,ga:users,ga:pageviews,ga:bouncerate,ga:AvgSessionDuration,ga:percentNewVisits,ga:timeOnPage", array());
        $mobile = $analytics->data_ga->get($ids, $from, $to, "ga:visits", array("dimensions" => "ga:deviceCategory"));
        $device = array();
        $total = 0;
        if($mobile->rows) {
            foreach($mobile->rows as $elt) {
                $device[$elt[0]] = $elt[1];
                $total += $elt[1];
            }
        }
        $desktop = array_key_exists("desktop", $device)?$device["desktop"]:0;

        ///////////////////////////// Tags   /////////////////////////////////////////////////
        $tags = $analytics->data_ga->get($ids, $from, $to, "ga:visits", array("dimensions" => "ga:eventAction","sort" => "-ga:visits"));

        $tag_structure = json_decode($campaign->structure);
        $tagtypes = array();
        $tagout = array();
        $clickList = array();
        if($tags->rows && $tag_structure){
            foreach ($tags->rows as $tagelt) {
                $titleUrl = null;
                $titleUrl = $this->searchUrl($tag_structure, $tagelt[0]);
                if($titleUrl == null) continue;
                $clickList[$titleUrl] = $tagelt[1];
            }
            foreach ($tags->rows as $tagelt) {
                $element = $this->searchTag($tag_structure, $tagelt[0]);
                if($element == null) continue;

                $title = $element->label . " [" .$element->id. "]";
                $type = (isset($element->type))?$element->type:null;

                $click = (array_key_exists($tagelt[0],$clickList))?$clickList[$tagelt[0]]:0;
                $tagout[] = array(
                        "title"=>$title,
                        "type"=>$type,
                        "view"=>$tagelt[1],
                        "click"=>$click
                );
                if($type != null) {
                  if(!isset($tagtypes[$type])) $tagtypes[$type] = array("view" => 0, "click" => 0);
                  $tagtypes[$type]["view"] += $tagelt[1];
                  $tagtypes[$type]["click"] += $click;
                }
            }
        }

        $bouncerate = round($visits->rows[0][3],2);
        $sessionDuration = round($visits->rows[0][4]);
        $NewSessionRate = round($visits->rows[0][5]);
        $desktopMobile = ($total > 0)?round($desktop/$total*100):0;

        if($debug_mode == "on" && \Auth::user()->isAdmin()) { 
          dd($tags->rows);
        }

        JavaScript::put([
              'from' => $from,
              'to' => $to,
              'visitschart_data' => $visitschart_data,
              'visitschart_categories' => $visitschart_categories,
              'country_data' => json_encode($Country_data),
              'Browsers_data' => $Browsers_data,
              'Devices_data' => $Devices_data,
              'countryTable_data' => $countryTable_data->rows,
              'TagsTable_data' => $tagout
        ]);

        return view('campaign.view',compact("campaign", "visits", "newsession", "returnsession", "bouncerate", "sessionDuration", "NewSessionRate", "desktopMobile", "tagtypes"));
    }

    public function getCountryCode($country_name) {
        $countries = array(
          'AF'=>'AFGHANISTAN', 'AL'=>'ALBANIA', 'DZ'=>'ALGERIA', 'AS'=>'AMERICAN SAMOA', 'AD'=>'ANDORRA',
          'AO'=>'ANGOLA', 'AI'=>'ANGUILLA', 'AQ'=>'ANTARCTICA', 'AG'=>'ANTIGUA AND BARBUDA', 'AR'=>'ARGENTINA',
          'AM'=>'ARMENIA', 'AW'=>'ARUBA', 'AU'=>'AUSTRALIA', 'AT'=>'AUSTRIA', 'AZ'=>'AZERBAIJAN', 'BS'=>'BAHAMAS',
          'BH'=>'BAHRAIN', 'BD'=>'BANGLADESH', 'BB'=>'BARBADOS', 'BY'=>'BELARUS', 'BE'=>'BELGIUM', 'BZ'=>'BELIZE',
          'BJ'=>'BENIN', 'BM'=>'BERMUDA', 'BT'=>'BHUTAN', 'BO'=>'BOLIVIA', 'BA'=>'BOSNIA AND HERZEGOVINA', 'BW'=>'BOTSWANA',
          'BV'=>'BOUVET ISLAND', 'BR'=>'BRAZIL', 'IO'=>'BRITISH INDIAN OCEAN TERRITORY', 'BN'=>'BRUNEI DARUSSALAM',
          'BG'=>'BULGARIA', 'BF'=>'BURKINA FASO', 'BI'=>'BURUNDI', 'KH'=>'CAMBODIA', 'CM'=>'CAMEROON', 'CA'=>'CANADA',
          'CV'=>'CAPE VERDE', 'KY'=>'CAYMAN ISLANDS', 'CF'=>'CENTRAL AFRICAN REPUBLIC', 'TD'=>'CHAD', 'CL'=>'CHILE',
          'CN'=>'CHINA', 'CX'=>'CHRISTMAS ISLAND', 'CC'=>'COCOS (KEELING) ISLANDS', 'CO'=>'COLOMBIA', 'KM'=>'COMOROS',
          'CG'=>'CONGO', 'CD'=>'CONGO, THE DEMOCRATIC REPUBLIC OF THE', 'CK'=>'COOK ISLANDS', 'CR'=>'COSTA RICA',
          'CI'=>'COTE D IVOIRE', 'HR'=>'CROATIA', 'CU'=>'CUBA', 'CY'=>'CYPRUS', 'CZ'=>'CZECH REPUBLIC', 'DK'=>'DENMARK',
          'DJ'=>'DJIBOUTI', 'DM'=>'DOMINICA', 'DO'=>'DOMINICAN REPUBLIC', 'TP'=>'EAST TIMOR', 'EC'=>'ECUADOR', 'EG'=>'EGYPT',
          'SV'=>'EL SALVADOR', 'GQ'=>'EQUATORIAL GUINEA', 'ER'=>'ERITREA', 'EE'=>'ESTONIA', 'ET'=>'ETHIOPIA',
           'FK'=>'FALKLAND ISLANDS (MALVINAS)', 'FO'=>'FAROE ISLANDS', 'FJ'=>'FIJI', 'FI'=>'FINLAND', 'FR'=>'FRANCE',
            'GF'=>'FRENCH GUIANA', 'PF'=>'FRENCH POLYNESIA', 'TF'=>'FRENCH SOUTHERN TERRITORIES', 'GA'=>'GABON', 'GM'=>'GAMBIA',
             'GE'=>'GEORGIA', 'DE'=>'GERMANY', 'GH'=>'GHANA', 'GI'=>'GIBRALTAR', 'GR'=>'GREECE', 'GL'=>'GREENLAND', 'GD'=>'GRENADA',
              'GP'=>'GUADELOUPE', 'GU'=>'GUAM', 'GT'=>'GUATEMALA', 'GN'=>'GUINEA', 'GW'=>'GUINEA-BISSAU', 'GY'=>'GUYANA', 'HT'=>'HAITI',
               'HM'=>'HEARD ISLAND AND MCDONALD ISLANDS', 'VA'=>'HOLY SEE (VATICAN CITY STATE)', 'HN'=>'HONDURAS', 'HK'=>'HONG KONG',
                'HU'=>'HUNGARY', 'IS'=>'ICELAND', 'IN'=>'INDIA', 'ID'=>'INDONESIA', 'IR'=>'IRAN, ISLAMIC REPUBLIC OF', 'IQ'=>'IRAQ',
                 'IE'=>'IRELAND', 'IL'=>'ISRAEL', 'IT'=>'ITALY', 'JM'=>'JAMAICA', 'JP'=>'JAPAN', 'JO'=>'JORDAN', 'KZ'=>'KAZAKSTAN',
                  'KE'=>'KENYA', 'KI'=>'KIRIBATI', 'KP'=>'NORTH KOREA', 'KR'=>'SOUTH KOREA', 'KW'=>'KUWAIT', 'KG'=>'KYRGYZSTAN',
                   'LA'=>'LAO PEOPLES DEMOCRATIC REPUBLIC', 'LV'=>'LATVIA', 'LB'=>'LEBANON', 'LS'=>'LESOTHO', 'LR'=>'LIBERIA',
                    'LY'=>'LIBYAN ARAB JAMAHIRIYA', 'LI'=>'LIECHTENSTEIN', 'LT'=>'LITHUANIA', 'LU'=>'LUXEMBOURG', 'MO'=>'MACAU',
                     'MK'=>'MACEDONIA, THE FORMER YUGOSLAV REPUBLIC OF', 'MG'=>'MADAGASCAR', 'MW'=>'MALAWI', 'MY'=>'MALAYSIA',
                      'MV'=>'MALDIVES', 'ML'=>'MALI', 'MT'=>'MALTA', 'MH'=>'MARSHALL ISLANDS', 'MQ'=>'MARTINIQUE', 'MR'=>'MAURITANIA',
                       'MU'=>'MAURITIUS', 'YT'=>'MAYOTTE', 'MX'=>'MEXICO', 'FM'=>'MICRONESIA, FEDERATED STATES OF', 'MD'=>'MOLDOVA,
                        REPUBLIC OF', 'MC'=>'MONACO', 'MN'=>'MONGOLIA', 'MS'=>'MONTSERRAT', 'MA'=>'MOROCCO', 'MZ'=>'MOZAMBIQUE',
                         'MM'=>'MYANMAR', 'NA'=>'NAMIBIA', 'NR'=>'NAURU', 'NP'=>'NEPAL', 'NL'=>'NETHERLANDS',
                          'AN'=>'NETHERLANDS ANTILLES', 'NC'=>'NEW CALEDONIA', 'NZ'=>'NEW ZEALAND', 'NI'=>'NICARAGUA', 'NE'=>'NIGER',
                           'NG'=>'NIGERIA', 'NU'=>'NIUE', 'NF'=>'NORFOLK ISLAND', 'MP'=>'NORTHERN MARIANA ISLANDS', 'NO'=>'NORWAY',
                            'OM'=>'OMAN', 'PK'=>'PAKISTAN', 'PW'=>'PALAU', 'PS'=>'PALESTINIAN TERRITORY, OCCUPIED', 'PA'=>'PANAMA',
                             'PG'=>'PAPUA NEW GUINEA', 'PY'=>'PARAGUAY', 'PE'=>'PERU', 'PH'=>'PHILIPPINES', 'PN'=>'PITCAIRN',
                              'PL'=>'POLAND', 'PT'=>'PORTUGAL', 'PR'=>'PUERTO RICO', 'QA'=>'QATAR', 'RE'=>'REUNION', 'RO'=>'ROMANIA', 'RU'=>'RUSSIA', 'RW'=>'RWANDA', 'SH'=>'SAINT HELENA', 'KN'=>'SAINT KITTS AND NEVIS', 'LC'=>'SAINT LUCIA', 'PM'=>'SAINT PIERRE AND MIQUELON', 'VC'=>'SAINT VINCENT AND THE GRENADINES', 'WS'=>'SAMOA', 'SM'=>'SAN MARINO', 'ST'=>'SAO TOME AND PRINCIPE', 'SA'=>'SAUDI ARABIA', 'SN'=>'SENEGAL', 'SC'=>'SEYCHELLES', 'SL'=>'SIERRA LEONE', 'SG'=>'SINGAPORE', 'SK'=>'SLOVAKIA', 'SI'=>'SLOVENIA', 'SB'=>'SOLOMON ISLANDS', 'SO'=>'SOMALIA', 'ZA'=>'SOUTH AFRICA', 'GS'=>'SOUTH GEORGIA AND THE SOUTH SANDWICH ISLANDS', 'ES'=>'SPAIN', 'LK'=>'SRI LANKA', 'SD'=>'SUDAN', 'SR'=>'SURINAME', 'SJ'=>'SVALBARD AND JAN MAYEN', 'SZ'=>'SWAZILAND', 'SE'=>'SWEDEN', 'CH'=>'SWITZERLAND', 'SY'=>'SYRIAN ARAB REPUBLIC', 'TW'=>'TAIWAN, PROVINCE OF CHINA', 'TJ'=>'TAJIKISTAN', 'TZ'=>'TANZANIA, UNITED REPUBLIC OF', 'TH'=>'THAILAND', 'TG'=>'TOGO', 'TK'=>'TOKELAU', 'TO'=>'TONGA', 'TT'=>'TRINIDAD AND TOBAGO', 'TN'=>'TUNISIA', 'TR'=>'TURKEY', 'TM'=>'TURKMENISTAN', 'TC'=>'TURKS AND CAICOS ISLANDS', 'TV'=>'TUVALU', 'UG'=>'UGANDA', 'UA'=>'UKRAINE', 'AE'=>'UNITED ARAB EMIRATES', 'GB'=>'UNITED KINGDOM', 'US'=>'UNITED STATES', 'UM'=>'UNITED STATES MINOR OUTLYING ISLANDS', 'UY'=>'URUGUAY', 'UZ'=>'UZBEKISTAN', 'VU'=>'VANUATU', 'VE'=>'VENEZUELA',
                               'VN'=>'VIET NAM', 'VG'=>'VIRGIN ISLANDS, BRITISH', 'VI'=>'VIRGIN ISLANDS, U.S.', 'WF'=>'WALLIS AND FUTUNA', 'EH'=>'WESTERN SAHARA', 'YE'=>'YEMEN', 'YU'=>'YUGOSLAVIA', 'ZM'=>'ZAMBIA', 'ZW'=>'ZIMBABWE', );

        $country_name = strtoupper($country_name);

        $key = array_search($country_name, $countries);

        return $key;

    }

    public function configure($id) {

      $campaign = \App\Campaign::find($id);

      $structure = json_encode(json_decode($campaign->structure), JSON_PRETTY_PRINT);

      return view('campaign.configure',compact("campaign","structure"));
    }

    public function configurePost(Request $request) {

      $structure = $request->input("structure");

      $id = $request->input("camp_id");

      \Validator::make($request->all(), [
            'structure' => 'required|json',
      ])->validate();

     // $structure = trim(preg_replace('/\s+/', '', $structure));

      $campaign = Campaign::find($id);

      $campaign->structure = $structure;

      $campaign->save();

      return redirect()->back()->with('message', 'The structure has been saved successfully.');

    }

    public function configureVideo($id) {

      $campaign = \App\Campaign::find($id);

      $configIns = Config::where("campaign_id",$id)->get();
      $config = array();

      foreach ($configIns as $conf) {
            $config[$conf->label] = $conf->value;
      }

      return view('campaign.configure-video',compact("campaign","config"));
    }

    public function configureVideoPost(Request $request) {

      $url = $request->input("video_elt");

      $id = $request->input("camp_id");

      \Validator::make($request->all(), [
            'video_elt' => 'required|url',
      ])->validate();

      $campaign = Campaign::find($id);

      $video = new Video();
      $video->url = $url;

      $campaign->videos()->save($video);

      return redirect()->back()->with('message', 'The video has been added successfully.');

    }

    public function configureAddPost(Request $request) {

      $elements = $request->except("_token","camp_id");

      $id = $request->input("camp_id");

      $campaign = Campaign::find($id);

      $configs = Config::where("campaign_id",$id)->get();

      foreach ($elements as $key => $value) {

            $val = $value;
            if($val == null) $val = "";

            Config::updateOrCreate(
                ['campaign_id' => $id, 'label' => $key],
                ['value' => $val]
            );
      }

      return redirect()->back()->with('message', 'The configuration has been added successfully.');

    }

    public function removeVideo(Request $request, $id)
    {
            $video = Video::find($id);

            $video->delete();


            return redirect()->back()->with('message', 'The video has been removed successfully.');
    }

    public function configureCss($id) {

      $campaign = \App\Campaign::find($id);

      return view('campaign.configure-css',compact("campaign"));
    }

    public function configureCssPost(Request $request) {

      $css = $request->input("css");

      $cssmobile = $request->input("cssmobile");

      $id = $request->input("camp_id");

      \Validator::make($request->all(), [
            'css' => 'required',
      ])->validate();

      //$css = trim(preg_replace('/\s+/', '', $css));
      //$cssmobile = trim(preg_replace('/\s+/', '', $cssmobile));

      $campaign = Campaign::find($id);

      $campaign->css = $css;
      $campaign->cssmobile = $cssmobile;

      $campaign->save();

      return redirect()->back()->with('message', 'The css has been saved successfully.');

    }

    public function configureAsset($id) {

      $campaign = \App\Campaign::find($id);

      $FilesIns = array(
          "PlayerWallpaper" => "", "PlayerBackground"=> "", "tag-indicator"=> "",
          "OverlayBackground"=> "", "PlayerIntro"=> "", "PlayerExit"=> ""
      );

      $structure = json_decode($campaign->structure);

      $thumbs =array();
      foreach ($structure as $elt) {
        $thumbs[] = $elt->id;
        $FilesIns[$elt->id] = array();
        if(!isset($elt->children)) continue;
        foreach ($elt->children as $child) {
            $thumbs[] = $child->id;
            $FilesIns[$child->id] = array();
        }
      }      

      $files = File::where("campaign_id", $id)->get();

      foreach ($FilesIns as $key => $fs) {
        $FilesIns[$key] = array('size' => "", 'path' => "");
      }

      foreach ($files as $fs) {
        $FilesIns[$fs->name] = array('size' => $fs->size, 'path' => File::BASE_S3 . env('AWS_BUCKET') . $fs->path);
      }

      return view('campaign.configure-asset',compact("campaign", "FilesIns", "thumbs"));
    }

    public function uploadFilePost(Request $request, $id, $name) {
        $file = $request->file('file');
        $img = \Image::make($file);

        $imageFileName = $name . "_" . time();

        File::where("campaign_id",$id)->where("name",$name)->delete();

        $s3 = \Storage::disk('s3');
        $filePath = '/campaigns/' . $id ."/". $imageFileName;
        $s3->put($filePath, file_get_contents($file), 'public');

        $fileIns = new File();
        $fileIns->name = $name;
        $fileIns->mime = $file->getMimeType();
        $fileIns->path = $filePath;
        $fileIns->width = $img->width();
        $fileIns->height = $img->height();
        $fileIns->size = $file->getSize();
        $fileIns->token = bin2hex(openssl_random_pseudo_bytes(24));
        $fileIns->campaign_id = $id;
        $fileIns->save();

        return  "OK";
    }

    public function delete($id) {
          $campaign = Campaign::find($id);

          $campaign->delete();

          //todo restriction

          return Redirect('home');
    }

    public function edit($id) {
          $campaign = Campaign::find($id);

          return view('campaign.edit',compact("campaign"));
    }

    public function doEdit(Request $request) {

      $this->validate($request, [
          'name' => 'required|max:255',
          'cgid' =>  'required|digits_between:8,10',
          'category_id' =>  'required',
      ]);


      $data = $request->except("_token");

      $campaign = Campaign::find($data["camp_id"]);

      $campaign->name = $data["name"];
      $campaign->cgid = $data["cgid"];
      $campaign->category_id = $data["category_id"];

      $campaign->save();
          
      return redirect()->back()->with('message', 'The campaign '.$data['name'].' has been updated.');
      
    }

    public static function dateRange($first, $last, $step = '+1 day', $format = 'Y/m/d') {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {

            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    public function searchTag($source, $title) {

          foreach ($source as $elt) {
              if($elt->id == $title) return $elt;

              if(isset($elt->children)) {
                  foreach ($elt->children as $subelt) {
                      if($subelt->id == $title) return $subelt;
                  }
              }

          }

          return null;
      }

      public function searchUrl($source, $title) {

          foreach ($source as $elt) {
              if($elt->url == $title) return $elt->id;

              if(isset($elt->children)) {
                  foreach ($elt->children as $subelt) {
                      if($subelt->url == $title) return $subelt->id;
                  }
              }

          }

          return null;
      }
}
