@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-lg-4">
                  <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('campaign-create') }}">Campaign</a></li>
                    <li class="active">#{{ $campaign->id }}</li>
                  </ol>
          </div>
          <div class="col-lg-4">
                <daterangepicker></daterangepicker>
          </div>
          <div class="btn-group col-lg-4">
            <button type="button" onclick="window.location.href='{{route('campaign-edit',["id" => $campaign->id])}}'" class="btn btn-default" aria-label="Configure Tags">
                    <span class="glyphicon glyphicon glyphicon-pencil" aria-hidden="true"> EDIT</span>
            </button>
            <button type="button" onclick="window.location.href='{{route('campaign-config',["id" => $campaign->id])}}'" class="btn btn-default" aria-label="Configure Tags">
                    <span class="glyphicon glyphicon glyphicon-th-list" aria-hidden="true"> CONFIGURE</span>
            </button>
            <button type="button" onclick="window.open('{{route('player-index',["name" => $campaign->name,"id" => $campaign->id])}}','_blank');" class="btn btn-default" aria-label="View Player">
                    <span class="glyphicon glyphicon glyphicon-eye-open" aria-hidden="true"> VIEW</span>
            </button>
          </div>
    </div>
    <div class="row">
      <div class="col-lg-4 col-sm-4">
        <div id="languette_1" class="languette grey1">
            <span>UNIQUE VISITS</span><span id="visitBadge" class="hbadge">{{ $visits->rows[0][0] }}</span>
        </div>
        <div id="languette_2" class="languette grey2">
            <span>PAGE VIEWS</span><span id="viewBadge" class="hbadge">{{ $visits->rows[0][2] }}</span>
        </div>
        <div id="languette_3" class="languette grey3">
            <span>BOUNCE RATE (%)</span><span id="bounceBadge" class="hbadge">{{ round($visits->rows[0][3],2) . " %" }}</span>
        </div>
        <div id="languette_4" class="languette grey4">
            <span>AVG SESSION DURATION (sec)</span><span id="sessionBadge" class="hbadge">{{ round($visits->rows[0][4]). " s" }}</span>
        </div>
        <div class="languette grey5" style='min-height: 400px'>
            <Pie id="pie" :new="{{$newsession}}" :return="{{$returnsession}}"></Pie>
        </div>
    </div>
    <div class="col-lg-8 col-sm-8">
        <Chart id="chart"  style="height:690px"></Chart>
    </div>
  <!--  <div class="col-lg-12 col-sm-12">
        <div id="geolocation" style="height:690px"></div>
    </div> -->
    <div class="col-lg-12 col-sm-12">
        <table id="countryTable" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>COUNTRY</th>
                    <th>VISITS</th>
                    <th>PAGES</th>
                    <th>AVG DURATION</th>
                    <th>BOUNCE</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="col-lg-6 col-sm-6 grey5">
        <div id="pieBrowser"></div>
    </div>
    <div class="col-lg-6 col-sm-6 grey5">
        <div id="pieDevice"></div>
    </div>
    <div class="row">
            <div class="col-lg-3 col-sm-3" style="padding:10px">
                    <easypie1 id="BounceRate" :percentage="{{$bouncerate}}"></easypie1>
                    <div style="text-align:center">Bounce Rate</div>
            </div>
            <div class="col-lg-3 col-sm-3" style="padding:10px">
                <easypie2 id="CTRate" :percentage="{{$sessionDuration}}"></easypie2>
                <div style="text-align:center">Click Through Rate</div>
            </div>
            <div class="col-lg-3 col-sm-3" style="padding:10px">
                <easypie3 id="NewSessionRate" :percentage="{{$NewSessionRate}}"></easypie3>
                <div style="text-align:center">% New Sessions</div>
            </div>
            <div class="col-lg-3 col-sm-3" style="padding:10px">
                <easypie3 id="desktopMobile" :percentage="{{$desktopMobile}}"></easypie3>
                <div style="text-align:center">Desktop Over Mobile</div>
            </div>
    </div>
    <div class="col-lg-12 col-sm-12">
        <table id="TagsTable" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>TAGS</th>
                    <th>VISITS</th>
                    <th>CLICKS</th>
                    <th>CLICK THROUGH RATE</th>
                </tr>
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    <div class="col-lg-12 col-sm-12">
        <table id="TagsTypeTable" class="table table-bordered table-striped table-hover">
            <thead>
                <tr>
                    <th>TYPES</th>
                    <th>VISITS</th>
                    <th>CLICKS</th>
                </tr>
                @foreach ($tagtypes as $key => $value)
                    <tr>
                        <td>{{ $key }}</td>
                        <td>{{ $value["view"] }}</td>
                        <td>{{ $value["click"] }}</td>
                    </tr>
                @endforeach
            </thead>
            <tbody>

            </tbody>
        </table>
    </div>
    </div>
</div>
@endsection
