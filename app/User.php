<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use App\Role;

class User extends Authenticatable
{
    use Notifiable;
    use EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles(){
        return $this->belongsToMany('App\Role');
    }

    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign');
    }

    public function isAdmin()
    {
        if(count($this->roles) > 0) {
          return ($this->roles[0]->name == "admin");
        } else {
          return false;
        }
    }

    public function getAccountAttribute()
    {
        if(count($this->roles) > 0) {
          return "<span>" . $this->roles[0]->display_name . "</span>";
        } else {
          return "<span class='label label-danger'>Not Defined</span>";
        }
    }

    public function getStatusAttribute()
    {
        if($this->confirmed) {
          return "<span class='label label-primary'>Activated</span>";
        } else {
          return "<span class='label label-danger'>Deactivated</span>";
        }

    }


}
