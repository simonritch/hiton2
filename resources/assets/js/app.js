
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('dropzone', require('./components/dropzone.vue'));

Vue.component('Pie', require('./components/Pie.vue'));

Vue.component('Chart', require('./components/Chart.vue'));

Vue.component('PieBrower', require('./components/PieBrower.vue'));

Vue.component('PieDevice', require('./components/PieDevice.vue'));

Vue.component('CountryTable', require('./components/CountryTable.vue'));
Vue.component('easypie2', require('./components/easypie2.vue'));
Vue.component('TagsTable', require('./components/TagsTable.vue'));

Vue.component('easypie1', require('./components/easypie1.vue'));
Vue.component('easypie3', require('./components/easypie3.vue'));
Vue.component('easypie4', require('./components/easypie4.vue'));

Vue.component('daterangepicker', require('./components/daterangepicker.vue'));


const app = new Vue({
    el: '#app',
});
