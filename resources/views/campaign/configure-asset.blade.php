@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-lg-12">
                  <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('campaign-create') }}">Campaign</a></li>
                    <li><a href="#">Configure - Assets</a></li>
                    <li class="active">#{{ $campaign->id }}</li>
                  </ol>
          </div>
          <div class="col-lg-12">

                <ul class="nav nav-pills nav-justified">
                      <li role="presentation"><a href="{{route('campaign-config',["id" => $campaign->id])}}">Tags / Events</a></li>
                      <li role="presentation"><a href="{{route('campaign-config-video',["id" => $campaign->id])}}">Videos and social links</a></li>
                      <li role="presentation"><a href="{{route('campaign-config-css',["id" => $campaign->id])}}">Style Sheets (CSS)</a></li>
                      <li role="presentation" class="active"><a href="{{route('campaign-config-asset',["id" => $campaign->id])}}">Images / Assets</a></li>
                </ul>
                <br/>

                @if($errors->first('css'))
                  <div class="alert alert-danger" role="alert">{{ $errors->first('css') }}</div>
                @endif
                @if(session()->has('message'))
                    <div class="alert alert-success">
                   {{ session()->get('message') }}
                   </div>
                @endif

    </div> 
    <div class="row">

                <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <b>General</b>
                    </div>
                  </div>
                </div>

    </div> 
    <div class="row">

                <div class="col-lg-2">
                        <dropzone id="PlayerWallpaper" pname="PlayerWallpaper" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns['PlayerWallpaper']['path'] }}" psize="{{ $FilesIns['PlayerWallpaper']['size'] }}"></dropzone>
                </div>
                <div class="col-lg-2">
                        <dropzone id="PlayerBackground" pname="PlayerBackground" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns['PlayerBackground']['path'] }}" psize="{{ $FilesIns['PlayerBackground']['size'] }}"></dropzone>
                </div>
                <div class="col-lg-2">
                        <dropzone id="tag-indicator" pname="tag-indicator" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns['tag-indicator']['path'] }}" psize="{{ $FilesIns['tag-indicator']['size'] }}"></dropzone>
                </div>
                <div class="col-lg-2">
                        <dropzone id="OverlayBackground" pname="OverlayBackground" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns['OverlayBackground']['path'] }}" psize="{{ $FilesIns['OverlayBackground']['size'] }}"></dropzone>
                </div>
                <div class="col-lg-2">
                        <dropzone id="PlayerIntro" pname="PlayerIntro" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns['PlayerIntro']['path'] }}" psize="{{ $FilesIns['PlayerIntro']['size'] }}"></dropzone>
                </div>
                <div class="col-lg-2">
                        <dropzone id="PlayerExit" pname="PlayerExit" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns['PlayerExit']['path'] }}" psize="{{ $FilesIns['PlayerExit']['size'] }}"></dropzone>
                </div>

    </div> 
    <br/>
    <div class="row">

                <div class="col-lg-12">
                  <div class="panel panel-default">
                    <div class="panel-body">
                      <b>Thumbnails</b>
                    </div>
                  </div>
                </div>

                @foreach ($thumbs as $tb)
                <div class="col-lg-2">
                      <dropzone id="{{ $tb }}" pname="{{ $tb }}" pcampaign="{{$campaign->id}}" purl="{{ $FilesIns[$tb]['path'] }}" psize="{{ $FilesIns[$tb]['size'] }}"></dropzone>
                 </div>
                @endforeach

    </div> 

          </div>
    </div>

</div>
@endsection
