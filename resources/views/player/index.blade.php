<!DOCTYPE html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8" />
    
    <title>{{$name}}: Powered by HitON</title>

    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link href="/favicon.ico" rel="icon" type="image/x-icon" />
    <link href="/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon" />

    <link rel="stylesheet" href="/jquery-ui.css" />
    <link rel="stylesheet" href="/player/css/{{$campaign->id}}/common.css" type="text/css" />
    <link rel="stylesheet" href="/player/css/{{$campaign->id}}/custom.css" type="text/css" />

    <style type="text/css">
        *::--webkit-media-controls-play-button {
          display: none!important;
          -webkit-appearance: none;
        }

        *::-webkit-media-controls-start-playback-button {
          display: none!important;
          -webkit-appearance: none;
        }
    </style>

</head>

<body>
    <section>
        <div id="p-container" class="container"></div>
    </section>

        <!-- Framework scripts -->
    <script src="https://code.jquery.com/jquery-3.2.1.js"></script>
    <script src="/jquery-ui.min.js"></script>
    <script src="/player_common/scripts/jquery.mousewheel.min.js"></script>
    <script src="/player_common/scripts/jquery.mCustomScrollbar.min.js"></script>
    
    <!-- Custom scripts -->
    <script type="text/javascript" src="/player_common/dist/encoded.js"></script>
    <script type="text/javascript" src="/player/scripts/{{$campaign->id}}/object.HotSpot.js"></script>
    <script type="text/javascript">
        jQuery.noConflict();
        jQuery(document).ready(function(e) {  
            var player = new ContentTheoryPlayer({
                "srcs":
                [
                    @foreach ($campaign->videos as $key => $video)
                    {
                        @if($key == 0)
                            points: jQuery.parseJSON('{!! $csv !!}'),
                        @else 
                            points: [],
                        @endif         
                        mp4:"{{ $video->url }}.m4v",
                        webm:"{{ $video->url }}.webm",
                        ogv:"{{ $video->url }}.ogv"
                    },
                    @endforeach
                    ], 
                "socialLinks":
                    {
                        @foreach ($configSocial as $key => $value)
                            {{ $key }}: "{{ $value }}",
                        @endforeach
                    },
                "frameRate": 25,
                "base_dir_img": "{{$base_dir_img}}",
                "PlayerIntro" : "{{$conf['PlayerIntro']}}",
                @foreach ($configAdd as $key => $value)
                        {{ $key }}: {{ ($value=="1")?"true":"false" }},
                @endforeach
                @if($context == "facebook")
                    "alert_msg" : "You are about to load a new page",
                @else
                    "alert_msg" : "You are about to open a new tab",
                @endif
            }); 

            jQuery("video").attr("webkit-playsinline", "");
            jQuery("video").attr("playsinline", "");

      
        
        });

        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', '{{$campaign->tracking_id}}']);
        _gaq.push(['_trackPageview']);

        (function() {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>
</body>
</html>