<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class File extends Model
{

	CONST BASE_S3 = "https://s3-ap-southeast-2.amazonaws.com/";

	use SoftDeletes;

    protected $table = 'files';

}
