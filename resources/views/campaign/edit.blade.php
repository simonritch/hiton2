@extends('layouts.app')

@section('content')
<div class="container">
    <ol class="breadcrumb">
        <li><a href="{{ route('home') }}">Home</a></li>
        <li><a href="{{ route('campaign-create') }}">Campaign</a></li>
        <li class="active">#{{ $campaign->id }}</li>
    </ol>
    <div class="row">
          <div class="col-lg-8 col-sm-8 col-lg-offset-2">
          @if(session()->has('message'))
              <div class="alert alert-success">
             {{ session()->get('message') }}
             </div>
          @endif
            <div class="panel panel-default">
                <div class="panel-body">
                      {{ Form::model($campaign, array('route' => 'campaign-do-edit')) }}

                      <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">

                           {{ Form::label('name', 'Name') }}
                              {{ Form::text('name', $campaign->name,array("class" => "form-control")) }}
                              @if ($errors->has('name'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('name') }}</strong>
                                  </span>
                              @endif
                      </div>

                      <div class="form-group{{ $errors->has('cgid') ? ' has-error' : '' }}">
                      {{ Form::label('cgid', 'Google View ID') }}
                              {{ Form::text('cgid', $campaign->cgid, array("class" => "form-control")) }}
                              @if ($errors->has('cgid'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('cgid') }}</strong>
                                  </span>
                              @endif
                      </div>

                      <div class="form-group{{ $errors->has('tracking_id') ? ' has-error' : '' }}">
                      {{ Form::label('tracking_id', 'Google Tracking ID') }}
                              {{ Form::text('tracking_id', old("tracking_id"), array("class" => "form-control")) }}
                              @if ($errors->has('tracking_id'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('tracking_id') }}</strong>
                                  </span>
                              @endif
                      </div>

                      <div class="form-group{{ $errors->has('cgid') ? ' has-error' : '' }}">
                      {{ Form::label('category_id', 'Category') }}
                              {{ Form::select('category_id', ['' => 'Select Value']+array_pluck(\App\Category::all(),'label','id'), $campaign->category_id, array("class" => "form-control")) }}
                              @if ($errors->has('category_id'))
                                  <span class="help-block">
                                      <strong>{{ $errors->first('category_id') }}</strong>
                                  </span>
                              @endif
                      </div>
                      {{ Form::hidden('camp_id', $campaign->id) }}      
                      {{ Form::submit('Update', array("class" => "btn btn-primary")) }}

                      {{ Form::close() }}
                </div>
              </div>
          </div>

    </div>
</div>
@endsection
