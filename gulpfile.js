var gulp = require('gulp');
gp_concat = require('gulp-concat'),
gp_rename = require('gulp-rename'),
gp_uglify = require('gulp-uglify');
gp_prettify = require('gulp-prettify');
gp_beautify = require('gulp-beautify');

gulp.task('concat-player', function(){
    return gulp.src(['resources/views/player/core/compiler_directive.blade.php', 'resources/views/player/core/object.blade.php', 'resources/views/player/core/VideoControls.blade.php', 'resources/views/player/core/ContentTheoryPlayer.blade.php'])
        .pipe(gp_concat('concat.js'))
        .pipe(gulp.dest('public/player_common/dist'))
        .pipe(gp_rename('encoded.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest('public/player_common/dist'));
});

gulp.task('concat-player-mobile', function(){
    return gulp.src(['resources/views/player/core/compiler_directive.blade.php', 'resources/views/player/core/object.blade.php', 'resources/views/player/core/VideoControls.blade.php', 'resources/views/player/core/ContentTheoryPlayerMobile.blade.php'])
        .pipe(gp_concat('concat_mobile.js'))
        .pipe(gulp.dest('public/player_common/dist'))
        .pipe(gp_rename('encoded_mobile.js'))
        .pipe(gp_uglify())
        .pipe(gulp.dest('public/player_common/dist'));
});

gulp.task('concat-player-prettify', function() {
  gulp.src('./public/player_common/dist/concat.js')
    .pipe(gp_beautify())
    .pipe(gulp.dest('public/player_common/dist')); 
});

gulp.task('default', ['concat-player'], function(){ console.log('Gulp done!') });