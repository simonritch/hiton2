<?php

namespace App\Http\Controllers;


use App\Campaign;
use App\Config;
use Illuminate\Http\Request;
use \App\File as File;

class PlayerController extends Controller
{


    
    public function index($name, $id, $context = "")
    {

            $base_dir = File::BASE_S3 . env("AWS_BUCKET");

            $campaign = Campaign::find($id);

            $configIns = Config::where("campaign_id",$id)->whereIn("label",["facebook","instagram","twitter"])->get();
            $configSocial = array();

            foreach ($configIns as $conf) {
                if($conf->value == "") continue;
                $configSocial[$conf->label] = $conf->value;
            }

            $configIns2 = Config::where("campaign_id",$id)->whereNotIn("label",["facebook","instagram","twitter"])->get();
            $configAdd = array();

            foreach ($configIns2 as $conf) {
                $configAdd[$conf->label] = $conf->value;
            }

            $csv = json_encode($this->csv($id)->getData());

            $base_dir_img = File::BASE_S3 . env("AWS_BUCKET") . "/common/images";

            $files = File::where("campaign_id",$id)->get();

            $conf = array();
            foreach($files as $file) {
                $conf[$file->name] = $base_dir . $file->path;
            } 

            $view = view('player.index',
                compact("campaign", "configSocial", "configAdd","name", "base_dir", "csv", "base_dir_img", "conf", "context"));

             return response($view)->header('X-Frame-Options', 'GOFORIT');

    }

    public function csv($id)
    {

            $campaign = Campaign::find($id);

            $structure = json_decode($campaign->structure);

            $data = array();
            $video_param = array("Orig_X" => 1280, "Orig_Y" => 720, "Video_X"=> 800, "Video_Y"=> 450);

            foreach ($structure as $key => $elt) {
                if(!isset($elt->framenumber)) continue;
                $frameamount = (isset($elt->frameamount))?$elt->frameamount:10;
                foreach (range(0, $frameamount-1) as $offset) {
                    $data[] = array($elt->framenumber + $offset, $key + 1, 1, 644, 1280, 134);
                }
            }

            $out = array();

            foreach ($data as $elmt) {
                $key = (string) $elmt[0];
                $points = array();
                $calc_1 = $video_param["Video_X"] / $video_param["Orig_X"] * $elmt[2];
                $calc_2 = $video_param["Video_Y"] / $video_param["Orig_Y"] * $elmt[5];
                $points[0] = array($calc_1, $calc_2);
                $calc_3 = $video_param["Video_X"] / $video_param["Orig_X"] * $elmt[4];
                $calc_4 = $video_param["Video_Y"] / $video_param["Orig_Y"] * $elmt[3];
                $points[1] = array($calc_3, $calc_4);
                $sub = array('ID' => $elmt[1], 'Points' => $points);
                $out = array_add($out, $key, array($sub));
            }

            return response()->json($out);
            

           // return view('player.objects',compact("campaign","structure"));

    }

    public function objects($id)
    {

            $campaign = Campaign::find($id);

            $base_dir = File::BASE_S3 . env("AWS_BUCKET");

            $thumbs_dir = $base_dir . "/campaigns/" . $id . "/thumbs";

            $structure = $campaign->structure;

            $structure = str_replace ( "[THUMBS_DIR]", $thumbs_dir , $structure); 

            $structure = json_decode($structure);

            $FilesList = array();
            $files = File::where("campaign_id",$id)->get();
            foreach ($files as $fs) {
                    $FilesList[$fs->name] = File::BASE_S3 . env('AWS_BUCKET') . $fs->path;
            }

/*
            foreach ($structure as $elt) {
                $elt->image = (isset($FilesList[$elt->id]))?($FilesList[$elt->id]):"";
                if(isset($elt->children))
                foreach ($elt->children as $children) {
                    $children->image = (isset($FilesList[$children->id]))?($FilesList[$children->id]):"";
                }
            } */

            foreach ($structure as $elt) {
                $elt->image = (isset($FilesList[$elt->id]))?(route('player-image', ['id' => $id, 'imgid' => $elt->id])):"";
                if(isset($elt->children))
                foreach ($elt->children as $children) {
                    $children->image = (isset($FilesList[$children->id]))?(route('player-image', ['id' => $id, 'imgid' => $children->id])):"";
                }
            } 

            $object = view('player.objects',compact("campaign","structure"));

            return response($object)->header('Content-Type', 'text/javacript');

    }

    public function core($id)
    {

            $campaign = Campaign::find($id);

            $content = view('player.core.encoded',compact("campaign"));

            $files = File::where("campaign_id",$id)->get();

            $base_dir = File::BASE_S3 . env("AWS_BUCKET");

            foreach($files as $file) {
                $content = str_replace ( "[" . $file->name . "]", $base_dir . $file->path , $content);
            }          

            return response($content)->header('Content-Type', 'text/javacript');

    }

    public function commonCss($id)
    {

            $campaign = Campaign::find($id);

            $base_dir_img = File::BASE_S3 . env("AWS_BUCKET") . "/common/images";

            $common_css = view('player.common_css',compact("base_dir_img"));

            return response($common_css)->header('Content-Type', 'text/css');

            //return view('player.css',compact("campaign","css"));

    }

    public function css($id)
    {

            $campaign = Campaign::find($id);

            if(\Agent::isMobile()) { 
                $css = $campaign->cssmobile;
            } else {
                $css = $campaign->css;
            }

            $files = File::where("campaign_id",$id)->get();

            $base_dir = File::BASE_S3 . env("AWS_BUCKET");

            foreach($files as $file) {
                $css = str_replace ( "[" . $file->name . "]", $base_dir . $file->path , $css);
            }

            $css = str_replace ( "[COMMON_IMG]", $base_dir . "/common/images" , $css);

            return response($css)->header('Content-Type', 'text/css');

            //return view('player.css',compact("campaign","css"));

    }

    public function image($id, $imgid)
    {
            $campaign = Campaign::find($id);

            $structure = $campaign->structure;

            $structure = json_decode($structure);           

            $imgIns = File::where("campaign_id",$id)->where("name",$imgid)->first();
            $base_dir = File::BASE_S3 . env("AWS_BUCKET");

            $img = \Image::make($base_dir . $imgIns->path);

            $tag = null;
            foreach ($structure as $elt) {
                if($elt->id == $imgid) {
                    $tag = $elt;
                }
                
            }

            if(isset($tag->mobile) && \Agent::isMobile()){

                    // draw filled red rectangle
                    $img->rectangle($tag->mobile->box_x1, $tag->mobile->box_y1, $tag->mobile->box_x2, $tag->mobile->box_y2, function ($draw) use($tag) {
                        $draw->background($tag->mobile->background);
                    });

                    
                    $img->text($tag->mobile->text, $tag->mobile->x, $tag->mobile->y, function($font) use($tag) {
                        $font->file(public_path() . "/fonts/helveticacondensed.ttf");
                        $font->size($tag->mobile->size);
                        $font->color($tag->mobile->color);
                    });

            }

            // create response and add encoded image data
            $response = \Response::make($img->encode('png'));    
            $response->header('Content-Type', 'image/png');
            return $response;
    }

    public function redirect(Request $request)
    {

            $url = $request->get("h");
            echo file_get_contents($url); 

    }
}
