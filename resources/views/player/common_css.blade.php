.container {
  padding-left: 0;
  padding-right: 0;
  margin-right: auto;
  margin-left: auto; }
  @media (min-width: 768px) {
    .container {
      width: 720px;
      padding-top: 30px; } }
  @media (min-width: 992px) {
    .container {
      width: 940px; } }

.frame {
  overflow: hidden;
  background-color: #FFF; }

.main {
  overflow: hidden; }
  @media (min-width: 768px) {
    .main {
      float: left;
      width: 580px; } }
  @media (min-width: 992px) {
    .main {
      width: 800px; } }

.player-wrapper {
  position: relative; }

.player {
  overflow: hidden;
  position: relative;
  width: 100%;
  padding-top: 56.25%; }
  .player video {
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    position: absolute;
    width: 100%;
    overflow: hidden; }

.info-list {
  display: block;
  list-style: none;
  margin: 0;
  padding: 0;
  overflow-x: hidden;
  overflow-y: auto;
  height: 0;
  background-color: #FFF;
  -webkit-transition: all 0.5s ease-out;
  transition: all 0.5s ease-out;
  height: 0;
  border-bottom: 1px solid #CCC; }
  @media (min-width: 768px) {
    .info-list {
      display: block;
      border-bottom: none; } }
  .info-list.active {
    height: 110px; }
    .info-list.active > li {
      height: 110px; }
  .info-list > li {
    -webkit-transition: all 0.5s ease-out;
    transition: all 0.5s ease-out;
    display: block;
    width: 100%;
    padding: 10px;
    overflow: hidden; }
    .info-list > li .thumb {
      float: left;
      width: 95px; }
      .info-list > li .thumb img {
        width: 100%;
        height: auto; }
    .info-list > li .desc {
      margin-left: 110px; }
      .info-list > li .desc p {
        margin: 0;
        line-height: 1.3; }
      .info-list > li .desc a {
        color: #000;
        text-decoration: none;
        text-transform: uppercase; }

.hitlist {
  width: 0;
  display: block;
  list-style: none;
  background-color: #FFF;
  background-image: url("https://s3-ap-southeast-2.amazonaws.com/hiton-server/common/images/shopping_bag.png");
  background-repeat: no-repeat;
  margin: 0;
  padding: 0;
  overflow-x: hidden;
  overflow-y: auto;
  font-size: 0;
  -webkit-transition: all 0.5s ease-out;
  transition: all 0.5s ease-out; }
  @media (min-width: 768px) {
    .hitlist {
      float: right;
      height: 440px; } }
  @media (min-width: 992px) {
    .hitlist {
      height: 560px; } }
  .hitlist.active {
    width: 100%;
    padding: 35px 0 5px 0; }
    @media (min-width: 768px) {
      .hitlist.active {
        width: 140px; } }
  .hitlist > li {
    -webkit-transition: all 0.5s ease-out;
    transition: all 0.5s ease-out;
    display: inline-block;
    width: 50%;
    padding: 5px 10px; }
    @media (min-width: 768px) {
      .hitlist > li {
        width: 100%; } }
    .hitlist > li img {
      width: 100%;
      height: auto; }

.topRightNav {
  background-color: transparent;
  background-image: url("https://s3-ap-southeast-2.amazonaws.com/hiton-server/common/images/hiton.svg");
  background-repeat: no-repeat;
  background-position: 50% 2px;
  top: 0;
  right: 0px;
  z-index: 3;
  position: absolute;
  min-width: 30px;
  max-height: 30px;
  overflow-y: hidden;
  -webkit-transition: max-height 0.5s ease-in-out, background-color 0.5s ease-in-out;
  transition: max-height 0.5s ease-in-out, background-color 0.5s ease-in-out; }
  .topRightNav:hover, .topRightNav:focus {
    background-color: rgba(0, 0, 0, 0.8);
    max-height: 150px; }
  .topRightNav ul {
    list-style: none;
    margin: 30px 0 0 0;
    padding: 0; }
    .topRightNav ul > li {
      margin: 0 0 5px 0;
      padding: 0; }
      .topRightNav ul > li a {
        display: block;
        text-align: center; }

a:-webkit-any-link {
  color: -webkit-link;
  cursor: auto;
  text-decoration: underline; }