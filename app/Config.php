<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Config extends Model
{

    protected $table = 'campaign_config';

    protected $fillable = ['campaign_id', 'label', 'value'];

}
