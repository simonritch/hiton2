@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
          <div class="col-lg-12">
                  <ol class="breadcrumb">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('campaign-create') }}">User</a></li>
                    <li class="active">#{{ $user->id }}</li>
                  </ol>
          </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="panel panel-default">
              <div class="panel-heading">
                  <a href="{{route('user-view',["id" => $user->id])}}">{{$user->name}}</a>
              </div>
                <div class="panel-body">
                      <span class="glyphicon glyphicon-envelope" style="margin-right:10px"></span>Email : {{$user->email}} <br/>
                      <span class="glyphicon glyphicon-user" style="margin-right:10px"></span>Account : {!! $user->account !!}<br/>
                      <span class="glyphicon glyphicon-off" style="margin-right:10px"></span>Status : <a href="{{route('user-status',["id" => $user->id])}}">{!! $user->status !!}</a><br/>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Campaigns
                    <button type="button" class="btn btn-default pull-right btn-xs" data-toggle="modal" data-target="#myModal">
                          <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </button>
                </div>
                <div class="panel-body">
                      <ul class="list-group">
                        @if(!$user->isAdmin()):
                              @foreach ($user->campaigns as $camp)
                                  <li class="list-group-item">{{$camp->name}}
                                        {{ Form::model($user, array('method' => 'post', 'route' => array('user-deny', $user->id))) }}
                                        {{ Form::hidden('campaign', $camp->id) }}
                                        <button type="submit" class="btn btn-danger pull-right btn-xs" aria-label="Remove" style="position:relative;top:-22px">
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                        </button>
                                        {{ Form::close() }}
                                  </li>
                              @endforeach
                        @else:
                              <li class="list-group-item list-group-item-success">Admin users have access to any campaign</li>
                        @endif
                      </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Grant access to a campaign</h4>
      </div>
      {{ Form::model($user, array('method' => 'post', 'route' => array('user-grant', $user->id))) }}
      <div class="modal-body">
         <div class="form-group">
              {{ Form::select('campaign', array_pluck(App\Campaign::all(), 'name', 'id') ,null, ['class' => 'form-control']) }}
          </div>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save</button>
      </div>
      {{ Form::close() }}
    </div>
  </div>
</div>
@endsection
