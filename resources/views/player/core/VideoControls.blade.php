/**
 * @constructor
 */
function VideoControls(videoPlayer, player_div_id)
{
    var self = this;

    self.trackbarHeight = 8;
    self.trackbarLoaded = false;

    self.videoPlayer = videoPlayer;
    self.controller = jQuery('#' + player_div_id + ' .controls');
    self.playbutton = jQuery('#' + player_div_id + ' .playpause button');
    self.trackbar = jQuery('#' + player_div_id + ' .trackbar');
    self.volumeSlider = jQuery('#' + player_div_id + ' .volumeSlider');
    self.volumeBtn = jQuery('#' + player_div_id + ' .volumeButton');
    self.stopper = 0;
    self.volume = 0.5;

    self.findX = function(el)
    {
        var left = 0;
        if(el.offsetParent)
        {
            do {
                left += el.offsetLeft;
            } while (el = el.offsetParent);
            return left;
        }
    }

    self.fixTime = function(t)
    {
        var s = t;
        var h = Math.floor(s / 3600);
        s = s % 3600;
        var m = Math.floor(s / 60);
        s = Math.floor(s % 60);
        if(s.toString().length < 2)
            s = "0" + s;
        if(m.toString().length < 2)
            m = "0" + m;
        return h + ":" + m + ":" + s;
    }

    self.muteVolume = function()
    {
        if(self.videoPlayer.muted)
        {
            self.videoPlayer.muted = false;
            self.volumeSlider.slider('value', self.volume);
            self.volumeBtn.removeClass('volume-mute');
        }
        else
        {
            self.videoPlayer.muted = true;
            self.volumeSlider.slider('value', '0');
            self.volumeBtn.addClass('volume-mute');
        }
    }

    // Constructor stuff

    self.videoPlayer.addEventListener('timeupdate', function(e) {
        if(!self.trackbarLoaded && self.videoPlayer.readyState)
        {
            var video_duration = self.videoPlayer.duration;
            self.trackbar.slider(
            {
                "value": 0,
                "step": 0.01,
                "orientation": "horizontal",
                "range": "min",
                "max": video_duration,
                "animate": true,
                "slide": function()
                {
                    self.seeksliding = true;
                },
                "stop": function(e,ui)
                {
                    self.seeksliding = false;
                    self.videoPlayer.currentTime = ui.value;
                }
            });
            self.trackbarLoaded = true;
        }
        
        if(!self.seeksliding)
        {
            self.trackbar.slider('value', self.videoPlayer.currentTime);
            jQuery('#'+player_div_id+' .display').html(self.fixTime(self.videoPlayer.currentTime) + "&nbsp;/&nbsp;");
        }
    }, false);

	self.videoPlayer.addEventListener('loadedmetadata', function(e) {
		jQuery('#'+player_div_id+' .duration').html(self.fixTime(self.videoPlayer.duration));
	}, false);

    self.videoPlayer.addEventListener('play', function(e) {
        self.playbutton.removeClass('play').addClass('pause');
    }, false);

    self.videoPlayer.addEventListener('pause', function(e) {
        self.playbutton.removeClass('pause').addClass('play');
    }, false);

    self.videoPlayer.addEventListener('ended', function(e) {
        self.playbutton.removeClass('pause').addClass('play');
    }, false);

    

    self.playbutton.click(function(e) {
        if(self.videoPlayer.paused) self.videoPlayer.play();
        else if(self.videoPlayer.ended)
        {
            self.videoPlayer.currentTime = 0;
            self.playbutton.removeClass('play').addClass('pause');
        }
        else
            self.videoPlayer.pause();
    });

    self.volumeSlider.slider({
        "value": 0.5,
        "orientation": "horizontal",
        "range": "min",
        "max": 1,
        "step": 0.05,
        "animate": true,
        "slide": function(e, ui) {
            self.videoPlayer.muted = false;
            self.volume = ui.value;
            self.videoPlayer.volume = ui.value;
        }
    });

    jQuery(self.volumeBtn).click(self.muteVolume);
}