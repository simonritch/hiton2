/**
 * @param {Object} options object with: player_div_id, srcs, player_width, player_height, clientId, socialLinks, frameRate
 * @constructor
 */
function ContentTheoryPlayer(options)
{
    var self = this;

    var settings = {
        "player_div_id": "p-container",
        "srcs": [],
        "player_width": 800,
        "player_height": 450,
        "clientId": "UA-35953914-2",
        "socialLinks": {},
        "frameRate": 25,
        "hitListOverlayHtml": "",
        "showProductDescriptionThumbs": false,
        "overlayClick": function() {}
    };

    self.settings = jQuery.extend(settings, options);

    self.player_div = jQuery('#'+self.settings["player_div_id"]);
    /**
     * @type {src:string,playing:boolean}
     * @private
     */
    self.video = self.player_div.find('video').get(0);
    self.controls = null;
    self.savedTimes = [];
    self.newFrameTime = new Date().getTime();
    self.lastFrameTime = new Date().getTime();
    self.repeatUpdateCount = 0;
    self.currentVideo = 0;

    self.show_front_transparent_layer = true;

    self.video_frame_cur = 0;
    self.productlist_showing_item = [];
    self.hotspotObjects = [];

    self.clickevent = (navigator.userAgent.match(/iPad/i)) ? "touchstart" : "mousedown";

    self.feed_parse = function() {

        self.hotspotObjects = getHotSpotObjects();

        self.player_div.find('.overlay').bind(self.clickevent, self.feed_update);
    }

    self.prepareForPlay = function() {
        self.lastFrameTime = new Date().getTime();
        self.player_div.find('.overlay').css({ "cursor" : "default" });
        self.show_front_transparent_layer = false;
        self.player_div.find('video').removeAttr('poster');
        _gaq.push(['_trackEvent', 'Play', self.video.src]);
        window.onbeforeunload = function storeData(){
            _gaq.push(['_trackEvent', 'Leave', 'Duration', isNaN(self.video.duration)?0:Math.round(self.video.duration*100), true]);
        };
    }

    /* Handles showing items on overlay for each frame */
    self.track_update = function(callingMyself) {
        if(callingMyself == null) callingMyself = false;
        if(!callingMyself) self.newFrameTime = new Date().getTime();
        video_frame_cur = Math.floor( self.video.currentTime * self.settings["frameRate"]);
        //console.log(video_frame_cur);

        //if (self.savedTimes.length <= 1)
        //{
            var objects_found = self.settings["srcs"][self.currentVideo] == null ? null : self.settings["srcs"][self.currentVideo]['points'][video_frame_cur];
            if (objects_found != null)  {
                var validHotspots = [];
                var autoItems = [];
                for (var i = 0; i < objects_found.length; i++) {
                    var TopL = objects_found[i]["Points"][0];
                    var BottomR = objects_found[i]["Points"][1];
                    var height = BottomR[1] - TopL[1];
                    var width = BottomR[0] - TopL[0];
                    var isAuto = false;

                    // Automatically "click" this object if any of its hotspots are auto (only hotspot is being appended)
                    for (var objectCount = 0; objectCount < self.hotspotObjects[self.currentVideo][objects_found[i]["ID"]].length; objectCount++) {
                        //console.log("ID: " + objects_found[i].ID + "| auto:" + self.hotspotObjects[self.currentVideo][objects_found[i].ID][objectCount].auto);
                        if(self.hotspotObjects[self.currentVideo][objects_found[i]["ID"]][objectCount].auto) {
                            isAuto = true;
                            autoItems.push(objects_found[i]["ID"]);
                        }
                    }

                    if(!isAuto) {
                        if(document.getElementById("hotspot"+objects_found[i]["ID"]) == null) {
                            self.player_div.find('.overlay').append(jQuery('<div class="hotspot" id="hotspot'+objects_found[i]["ID"]+'" style="top: '+ TopL[1] +'px; left:'+ TopL[0] +'px; height: '+ height +'px; width: '+ width +'px; line-height: '+ height +'px; ">'+(isAuto ? "" : '<div class="animation"> </div>')+'</div>'));
                            setTimeout(function(){self.player_div.find('.overlay .animation').css("visibility","hidden");},1600);
                        } else {
                            jQuery("#hotspot"+objects_found[i]["ID"]).css({"top":TopL[1]+'px', "left":TopL[0]+'px', "height": height+'px', "width":width+'px',"line-height":height+'px'});
                        }
                        validHotspots.push("hotspot"+objects_found[i]["ID"]);
                    }
                }
                self.showItemDetails(autoItems);
                self.player_div.find('.overlay .hotspot').each(function() {
                    if(validHotspots.indexOf(this.id) < 0) jQuery(this).remove();
                });
            } else {
                self.player_div.find('.overlay').empty();
                self.player_div.find('.overlay').css({cursor : "default"});
            }
        //}
        //else self.player_div.find('.overlay').empty();

        //console.log("self: " + callingMyself + " new: " + self.newFrameTime + " last: " + self.lastFrameTime + " floor: " + Math.floor((self.newFrameTime - self.lastFrameTime)/50));
        if(callingMyself) self.repeatUpdateCount++;
        if(callingMyself && self.repeatUpdateCount <= Math.floor(240/50)) {
            //console.log("repeating " + self.repeatUpdateCount);
            setTimeout(function(){self.track_update(true);}, 50);
        }
        if(!callingMyself) {
            if(self.newFrameTime - self.lastFrameTime > 100) {
                //console.log("time: " + (self.newFrameTime - self.lastFrameTime));
                self.repeatUpdateCount = 0;
                setTimeout(function(){self.track_update(true);}, 50);
            }
            self.lastFrameTime = new Date().getTime();
        }
    }

    /* Handles the click on the overlay */
    self.feed_update = function(e) {
        var hitListOverlay = jQuery('.hitListOverlay');
        if(hitListOverlay.length > 0 && (hitListOverlay[0] == e.target || jQuery.contains(hitListOverlay[0], e.target))) return;
        if (!self.show_front_transparent_layer) {
            self.settings["overlayClick"]();
            var video =  self.player_div.find('video').offset();
            var x = ((typeof event !== 'undefined' && event.targetTouches != null) ? event.targetTouches[0].pageX : e.pageX) - video.left;
            var y = ((typeof event !== 'undefined' && event.targetTouches != null) ? event.targetTouches[0].pageY : e.pageY) - video.top;

            if (self.settings["srcs"][self.currentVideo]['points'][video_frame_cur] != null) self.showItemDetails(self.mouseInObject(x,y, self.settings["srcs"][self.currentVideo]['points'][video_frame_cur]));
            else console.log("No objects in current (clicked) frame");
        } else {
            self.video.play();
            self.show_front_transparent_layer = false;

            _gaq.push(['_trackEvent', 'Start', self.video.src]);
        }
    }

    self.showItemDetails = function(object_ids) {
        for (var i = 0; i < object_ids.length; i++) {
            for (var j = 0; j < self.hotspotObjects[self.currentVideo][object_ids[i]].length; j++) {
                var objectId = object_ids[i] + "_" + j;
                if (self.hotspotObjects[self.currentVideo][object_ids[i]][j].type != null) {
                    if(self.hotspotObjects[self.currentVideo][object_ids[i]][j].clicked) {
                        // If it's already clicked, just add it back to the top of the list
                        var hitItem = self.player_div.find('.hitList'+objectId);
                        hitItem.parent().prepend(hitItem);
                    } else {
                        //console.log("Hasn't been clicked yet");
                        _gaq.push(['_trackEvent', 'HotspotClick', objectId]);
                        self.hotspotObjects[self.currentVideo][object_ids[i]][j].clicked = true;
                        console.log(self.hotspotObjects[self.currentVideo][object_ids[i]][j].type);
                        switch(self.hotspotObjects[self.currentVideo][object_ids[i]][j].type) {
                            case "HitListItem":
                                self.player_div.find('.hitlist').prepend(jQuery('<li class="hitList'+objectId+'"></li>').prepend(jQuery('<a href="' +self.hotspotObjects[self.currentVideo][object_ids[i]][j].url +'" target="_blank"><img class="productlist" src="'+ self.hotspotObjects[self.currentVideo][object_ids[i]][j].image+'" /></a>').bind(self.clickevent, function(){ _gaq.push(['_trackEvent', 'HitListClick', this.href]); self.video.pause(); if(typeof window["hitListItemHook"] === "function") window["hitListItemHook"](); })).fadeIn(800));
                                self.player_div.find(' .contentRight').show().animate({width: "139px"}, 300 );
                                self.player_div.find(".hitlist").addClass('active');
                                jQuery('.hitlist > li:first-child a').on('click', function(e){
									if(self.settings.alert_msg.length && jQuery(window).width() < 768) {
										e.preventDefault();
										var r = confirm(self.settings.alert_msg);
										if (r == true) {
											window.open(jQuery(this).attr('href'));
											self.settings.alert_msg = '';
										}
									}
								});
                                break;
                            case "HitListSummaryItem":
                                self.player_div.find('.hitlist').prepend(jQuery('<li class="HitListSummaryItem" style="display:none"></li>').prepend(jQuery('<a href="' +self.hotspotObjects[self.currentVideo][object_ids[i]][j].url +'" target="_blank"><img class="productlist" src="'+ self.hotspotObjects[self.currentVideo][object_ids[i]][j].image+'" /></a>').bind(self.clickevent, function(){ _gaq.push(['_trackEvent', 'HitListSummaryClick', this.href]); self.video.pause(); if(typeof window["hitListItemHook"] === "function") window["hitListItemHook"](); })).fadeIn(800));
                                break;
                            case "HitListSubvideoItem":
                                var vidId = self.hotspotObjects[self.currentVideo][object_ids[i]][j].subvideoId;
                                self.player_div.find('.hitlist').prepend(jQuery('<li class="hitList'+objectId+'"></li>').prepend(
                                    jQuery('<img class="productlist" src="'+ self.hotspotObjects[self.currentVideo][object_ids[i]][j].image+'" style="cursor:pointer" />').data('vidId', vidId).bind(self.clickevent, function(){
                                        var savedId = jQuery(this).data('vidId');
                                        var spotId = objectId;
                                        self.video.pause();
                                        if(typeof window["hitListSubvideoItemHook"] === "function") window["hitListSubvideoItemHook"]();
                                        self.showReturnToMain(spotId, true)
                                        self.changeVideo(savedId);
                                        _gaq.push(['_trackEvent', 'ChangeVidClick', savedId+","+objectId]);
                                    })
                                ).fadeIn(800));
                                self.player_div.find('.contentRight').show().animate({width: "139px"}, 300 );
                                self.player_div.find(".hitlist").addClass('active');
                                break;
                            case "ProductDescriptionItem":
                                self.addProductDetail(self.hotspotObjects[self.currentVideo][object_ids[i]][j]);
                                if (self.hotspotObjects[self.currentVideo][object_ids[i]][j]['_description'] != "")
                                    self.player_div.find('.bottomslide').animate({top:self.settings["player_height"]+"px"}, 500)
                                self.player_div.find(".info-list").addClass('active');
                                break;
                            case "SubvideoItem":
                                var vidId = self.hotspotObjects[self.currentVideo][object_ids[i]][j].branchId;
                                self.player_div.find('.info-list').prepend(
                                    jQuery('<li class="subvideoItem"></li>').addClass("hitList"+objectId).append(
                                        jQuery('<div></div>').addClass("productDetail detailImage").append(
                                            jQuery('<img></img>').attr({"src":self.hotspotObjects[self.currentVideo][object_ids[i]][j].image,"alt":self.hotspotObjects[self.currentVideo][object_ids[i]][j].title}).addClass("image subVid"+vidId)
                                        ).bind(self.clickevent,function(){
                                                var spotId = objectId;
                                                if(typeof window["subvideoItemHook"] === "function") window["subvideoItemHook"]();
                                                self.video.pause();
                                                self.showReturnToMain(spotId, true);
                                                self.changeVideo(vidId);
                                                _gaq.push(['_trackEvent', 'ChangeVidClick', vidId+","+objectId]);
                                        }).addClass("subVid"+vidId)
                                    )
                                    .append(
                                        jQuery('<div></div>').addClass("desc").append(
                                            jQuery('<p></p>').html(self.hotspotObjects[self.currentVideo][object_ids[i]][j].description).prepend('<br />').prepend(
                                                jQuery('<span></span>').text(self.hotspotObjects[self.currentVideo][object_ids[i]][j].title).bind(self.clickevent,function(){
                                                    if(typeof window["subvideoItemHook"] === "function") window["subvideoItemHook"]();
                                                    self.video.pause();
                                                    self.showReturnToMain(objectId, true)
                                                    self.changeVideo(vidId);
                                                    _gaq.push(['_trackEvent', 'ChangeVidClick', vidId+","+objectId]);
                                                }).addClass("subVid"+vidId)
                                            )
                                        )
                                    )
                                );
                                //self.player_div.find('.bottomslide .scroll-pane').mCustomScrollbar({theme:"dark-thick"});
                                if (self.hotspotObjects[self.currentVideo][object_ids[i]][j].description != "")
                                    self.player_div.find('.bottomslide').animate({top:self.settings["player_height"]+"px"}, 500)

                                self.player_div.find(".hitlist").addClass('active');
                               break;
                            case "SubvideoHotspot":
                                var vidId = self.hotspotObjects[self.currentVideo][object_ids[i]][j].branchId;
                                var returnId = self.hotspotObjects[self.currentVideo][object_ids[i]][j].returnId;
                                self.video.pause();
                                self.showReturnToMain(objectId, false, returnId)
                                self.changeVideo(vidId);
                                _gaq.push(['_trackEvent', 'ChangeVidClick', vidId+","+objectId]);
                                self.player_div.find(".hitlist").addClass('active');
                                return;
                            case "EmailItem":
                                window.location.href = self.hotspotObjects[self.currentVideo][object_ids[i]][j].email;
                                self.video.pause();
                                _gaq.push(['_trackEvent', 'EmailClick', vidId+","+objectId]);
                                return;
                            case "UrlItem":
                                window.open(self.hotspotObjects[self.currentVideo][object_ids[i]][j].url);
                                self.video.pause();
                                _gaq.push(['_trackEvent', 'UrlItemCLick', vidId+","+objectId]);
                                break
                            case "MultiItem":
                                var descArea = jQuery('<p></p>');
                                for(var itemCount in self.hotspotObjects[self.currentVideo][object_ids[i]][j].items) {
                                    descArea.append(jQuery('<a href="' +self.hotspotObjects[self.currentVideo][object_ids[i]][j].items[itemCount].url +'" target="_blank"><img class="productlist" src="'+ self.hotspotObjects[self.currentVideo][object_ids[i]][j].items[itemCount].image+'" /></a>').bind(self.clickevent, function(){ _gaq.push(['_trackEvent', 'MultiListClick', this.href]);}));
                                }
                                self.player_div.find('.info-list').prepend(
                                    jQuery('<li class="multiItem"></li>').addClass("hitList"+objectId).append(
                                        jQuery('<div></div>').addClass("productDetail detailImage").append(
                                            jQuery('<a></a>').attr({"target":"_blank","href":self.hotspotObjects[self.currentVideo][object_ids[i]][j].url}).append(
                                                jQuery('<img></img>').attr({"src":self.hotspotObjects[self.currentVideo][object_ids[i]][j].image,"alt":self.hotspotObjects[self.currentVideo][object_ids[i]][j].title}).addClass("image")
                                            ).bind(self.clickevent,function(){
                                                    self.video.pause();
                                                    _gaq.push(['_trackEvent', 'MultiItemClick', this.href]);
                                            })
                                        )
                                    ).append(
                                        jQuery('<div></div>').addClass("desc").append(descArea)
                                    )
                                );
                                //self.player_div.find('.bottomslide .scroll-pane').mCustomScrollbar({theme:"dark-thick"});
                                if (self.hotspotObjects[self.currentVideo][object_ids[i]][j].description != "")
                                    self.player_div.find('.bottomslide').animate({top:self.settings["player_height"]+"px"}, 500)
                                self.player_div.find(".hitlist").addClass('active');
                                break;
                            default: console.log("Unknown hotspot type found, index: " + objectId);
                        }
                    }
                } else {
                    console.log("Cannot determine object type for ID " + objectId);
                }
            }
        }
    }

    self.addProductDetail = function(theObject) {
        if(theObject.url == null) theObject.url = " ";

        var listItemTag = jQuery('<li class="productDetailItem"></li>').addClass("hitList"+theObject.ID);
        var linkTag1 = jQuery('<img></img>').attr({"src":theObject.image,"alt":theObject.url}).addClass("image");
        var linkTag2 = null;
        if(theObject.url == null || theObject.url == "") {
            linkTag2 = jQuery('<span></span>').text(theObject.title).addClass("subVid");
        } else {
            linkTag1 = jQuery('<a class="thumb"></a>').attr({'href':theObject.url, 'target':'_blank'}).append(linkTag1).bind(self.clickevent,function(){self.video.pause()});
            linkTag2 = jQuery('<a></a>').attr({'href':theObject.url, 'target':'_blank'}).text(theObject.title).bind(self.clickevent,function(){self.video.pause()});
        }
        var imgTag = jQuery('<div></div>').addClass("productDetail detailImage").append(linkTag1);
        var textTag = jQuery('<div></div>').addClass("desc");
        if(theObject.url != null || theObject.url != "")
        {
            linkTag1.bind(self.clickevent, function(){ _gaq.push(['_trackEvent', 'ProductDescription1Click', this.href]); });
            linkTag2.bind(self.clickevent, function(){ _gaq.push(['_trackEvent', 'ProductDescription2Click', this.href]); });
        }
        textTag.append(jQuery('<p></p>').html(theObject.description)).prepend(linkTag2);
        listItemTag.append(imgTag);
        listItemTag.append(textTag);
        self.player_div.find('.info-list').prepend(listItemTag);
        //self.player_div.find('.bottomslide .scroll-pane').mCustomScrollbar({theme:"dark-thick"});
    }

    self.showReturnToMain = function(objectId, hideProductDetail, returnId) {
        jQuery('.returntomain').remove();
        var returnToMainTag = jQuery('<div></div>');
        var imgTag = jQuery('<div></div>').addClass("desc").append(jQuery('<img></img>').attr({"src":self.settings["base_dir_img"]+"/return_to_main.png","alt":"Return to Main","title":"Return to Main"}).addClass("image"));
        imgTag.find('img').bind(self.clickevent, function(){
            if(typeof window["returnToMainHook"] === "function") window["returnToMainHook"]();
            self.player_div.find('.info-list').find(".returntomain").remove();
            self.player_div.find('.info-list').find(".hitList"+objectId).show();
            self.player_div.find('.bottomslide').animate({top:self.settings["player_height"]+"px"}, 500)
            self.changeVideo(returnId == null ? -1 : returnId, returnId != null);
        });
        var textTag = jQuery('<div></div>').addClass("desc");
        textTag.append(jQuery('<p>Click the thumbnail to return to the main film</p>'));
        returnToMainTag.append(imgTag);
        returnToMainTag.append(textTag);
        //self.player_div.find('.bottomslide').prepend(returnToMainTag);
        var listItemTag = jQuery('<li></li>').addClass("returntomain");
        listItemTag.prepend(returnToMainTag);
        self.player_div.find('.info-list').prepend(listItemTag);
        self.player_div.find('.info-list').find(".hitList"+objectId).hide();
        self.player_div.find('.bottomslide').animate({top:self.settings["player_height"]+"px"}, 500);
        self.player_div.find(".info-list").addClass('active')
    }

    self.showReplay = function() {
        if(self.savedTimes.length <= 1) return;

        self.player_div.find('.overlay').empty();
        self.player_div.find('.overlay').prepend(jQuery('<div class=\"replay\"></div><div class=\"returntomain\"></div>'));
        self.player_div.find('.replay').css({
            'top': Math.ceil(self.settings["player_height"]/2-40/2)+'px',
            'left': Math.ceil(self.settings["player_width"]/2-119/2-119)+'px'
            });
        self.player_div.find('.returntomain').css({
            'top': Math.ceil(self.settings["player_height"]/2-40/2)+'px',
            'left': Math.ceil(self.settings["player_width"]/2-119/2+119)+'px'
            });

        self.player_div.find('.replay').bind(self.clickevent, function(){
            self.video.currentTime = 0;
            self.player_div.find('.replay').hide();
            self.player_div.find('.returntomain').hide();
            if(!self.video.playing) self.video.play();
        });
        self.player_div.find('.returntomain').bind(self.clickevent, function(){self.changeVideo(-1)});

        self.player_div.find('.replay').show();
        self.player_div.find('.returntomain').show();

        self.player_div.find(".info-list").addClass('active');
    }

    self.mouseInObject = function(x, y, track_objects) {
        var object_id = [];
        for (var i = 0; i < track_objects.length; i++) {
            if (x >= track_objects[i]["Points"][0][0] && x <= track_objects[i]["Points"][1][0] && y >= track_objects[i]["Points"][0][1] && y <= track_objects[i]["Points"][1][1])
                object_id.push(track_objects[i]["ID"]);
        }
        return object_id;
    }

    self.setVideo = function(videoId) {
        self.currentVideo = videoId;
        self.savedTimes.push({id:videoId,time:0});

        self.player_div.find('.overlay').empty();
        self.video.pause();
        if(self.settings["srcs"][videoId]['mp4'] != null)
            self.player_div.find('source.mp4').attr('src', self.settings["srcs"][videoId]['mp4']);
        if(self.settings["srcs"][videoId]['webm'] != null)
            self.player_div.find('source.webm').attr('src', self.settings["srcs"][videoId]['webm']);
        if(self.settings["srcs"][videoId]['ogv'] != null)
            self.player_div.find('source.ogv').attr('src', self.settings["srcs"][videoId]['ogv']);

        self.video.load();
    }

    self.changeVideo = function(videoId, showFinishedList) {
        if(videoId == -1) {
            while(self.savedTimes.length > 1) self.savedTimes.pop();
            var oldVid = self.savedTimes.pop();
            self.setVideo(oldVid.id);
            self.controls.trackbarLoaded = false; // reload the trackbar
            self.video.addEventListener('loadedmetadata', function(){ self.video.currentTime = oldVid['time']; self.video.play(); });
        } else {
            self.savedTimes[self.savedTimes.length-1]['time'] = self.video.currentTime;
            self.setVideo(videoId);
            self.controls.trackbarLoaded = false; // reload the trackbar
            self.video.addEventListener('loadedmetadata', function(){ self.video.currentTime = 0; self.video.play(); });
            self.video.removeEventListener('ended', self.showFinishedHitList, false);
            self.video.addEventListener('ended', self.showReplay, false );
        }
        if(videoId == -1 || showFinishedList == true) {
            self.video.removeEventListener('ended', self.showReplay, false);
            self.video.addEventListener('ended', self.showFinishedHitList, false);
        }
    }

    self.showFinishedHitList = function() {
        _gaq.push(['_trackEvent', 'End', self.video.src]);
        self.video.pause();
        if(typeof window["showFinishedHitlistHook"] === "function") window["showFinishedHitlistHook"]();

        setTimeout(function(){
            //self.showFinishedPopup();
            self.showFinishedOverlay()
        },200);
    }

    self.showFinishedOverlay = function() {
		var e = self.player_div.find(".overlay");
		e.empty();
		if(jQuery(window).width() < 768) {
			var i = jQuery('<div class="playerExitOverlay"></div>');
			e.append(i);
		}
		else
			{
			var i = jQuery('<div class="hitListOverlay"></div>');
			if (self.settings.showProductDescriptionThumbs) {
				var s = "<ul>";
				self.player_div.find(".info-list .productDetailItem .detailImage a").each(function() {
					s += "<li>" + this.outerHTML + "</li>"
				}), s += "</ul>", i.append(s)
			} else if (self.settings.showHitListSummaryItemOnly) {
				var a = self.player_div.find(".hlsBox ul");
				i.append(a.clone()), console.log("showHitListSummaryItemOnly")
			} else {
				var n = self.player_div.find(".hitlist ul");
				i.append(n.clone())
			}
			e.append(i);
			// "" != t.settings.hitListOverlayHtml && i.append(t.settings.hitListOverlayHtml);
			// var o = jQuery('<input class="resumefilm" type="button" />');
			// i.append(jQuery('<div style="clear:both"></div>').append(o)), o.bind(t.clickevent, function() {
			// 	"function" == typeof window.resumeFilmHook && window.resumeFilmHook(), t.video.currentTime = 0, t.player_div.find(".replay").hide(), t.player_div.find(".returntomain").hide(), t.video.playing || t.video.play()
			// }), i.find("input").css({
			// 	width: "108px",
			// 	"background-color": "black",
			// 	color: "white",
			// 	"margin-left": "5px"
			// }), i.find("img").each(function() {
			// 	jQuery(this).attr("src", jQuery(this).attr("src"))
			// }), e.append(i), i.mCustomScrollbar({
			// 	theme: "dark-thick"
			// })
		}
	}

    self.showFinishedPopup = function() {
        //console.log("Opening popup");

        var w = window.open("","Hit List","width=800,height=400,location=no,menubar=no,status=no,titlebar=no,toolbar=no");
        var newWindowDoc = jQuery(w.document.body);
        var hitElements = self.player_div.find('.hitlist');
        newWindowDoc.empty();
        newWindowDoc.append(jQuery('<h2 style="background:black;color:white;text-align:center;text-transform:uppercase">My Hit List</h2>'));
        newWindowDoc.append(hitElements);
        newWindowDoc.find('ul').css("width","100%");
        newWindowDoc.find('li').css({"width":"120px","float":"left","border":"1px solid gray","margin":"5px"});
        newWindowDoc.find('li span').css({"display":"block","border-top":"1px solid gray","text-align":"right","color":"black"}).html('<span><a href="#">f</a></span> <span><a href="#">f</a></span> <span><a href="#">f</a></span> <span><a href="#">f</a></span>');
        newWindowDoc.append(jQuery('<div style="clear:both"><input type="submit" value="Save" /><input type="submit" value="Share" /></div>'));
        newWindowDoc.find('input').css({"width":"118px","background-color":"black","color":"white","margin-left":"5px"});
        newWindowDoc.find('img').each(function(){
            jQuery(this).attr('src', location.protocol + "//" + location.hostname + location.pathname + jQuery(this).attr('src'));
        });
    }

    // --------- Constructor

    jQuery('#'+self.settings["player_div_id"]).append(jQuery('<div class="frame"><div class="main" ><div class="player-wrapper"><div class="topRightNav"><ul><li><a href="#" class="icon1"></a></li>'+(self.settings["socialLinks"]['facebook']==null?'':'<li><a href="'+self.settings["socialLinks"]['facebook']+'" target="_blank" class="fb"><img src="'+self.settings["base_dir_img"]+'/facebook.svg" alt="f" title"facebook" /></a></li>')+(self.settings["socialLinks"]['twitter']==null?'':'<li><a href="'+self.settings["socialLinks"]['twitter']+'" target="_blank" class="tw"><img src="'+self.settings["base_dir_img"]+'/twitter.svg" alt="t" title"Twitter" /></a></a></li>')+(self.settings["socialLinks"]['instagram']==null?'':'<li><a href="'+self.settings["socialLinks"]['instagram']+'" target="_blank" class="instagram"><img src="'+self.settings["base_dir_img"]+'/instagram.svg" alt="instagram" title"instagram" /></a></li>')+(self.settings["socialLinks"]['music']==null?'':'<li><a href="'+self.settings["socialLinks"]['music']+'" target="_blank" class="music"><img src="'+self.settings["base_dir_img"]+'/itunes.svg" alt="music" title"itunes" /></a></li>')+'</ul></div><div class="player"><video class="video" poster="'+self.settings["PlayerIntro"]+'" preload="auto" controls><source class="video mp4" src="access/blocker.php?video=mp4&apikey=10286172648a90fdac71e86f9278ce963ed31d9808872da26362c3119684c660" type="video/mp4" /><source class="video webm" src="'+self.settings["srcs"][0]['webm']+'" type="video/webm" /><source class="video ogv" src="'+self.settings["srcs"][0]['ogv']+'" type="video/ogg" />Your browser does not support HTML5 video</video><div class="overlay"></div></div></div><ul class="info-list"></ul></div><ul class="hitlist"></ul></div>'));
    jQuery('.scroll-pane').mCustomScrollbar({theme:"dark-thick"});
    jQuery('.bottomslide').css({top: "-500px"});

    self.player_div = jQuery('#'+self.settings["player_div_id"]);
    self.video = self.player_div.find('video')[0];
    self.controls = new VideoControls(self.video, 'container');
    self.player_div.find('.videoHitList img').css("cursor","pointer").bind(self.clickevent, self.showFinishedHitList);

    self.setVideo(0);
    self.feed_parse();

    self.video.addEventListener('timeupdate', function(){ self.track_update(false); }, false);

    self.video.addEventListener('play', self.prepareForPlay, false);

    self.video.addEventListener('ended', self.showFinishedHitList, false);

    self.player_div.find('.topRightNav a').bind(self.clickevent, function(){
        if(this.href != '#') _gaq.push(['_trackEvent', 'SocialClick', this.href]);
    });

    self["video"] = self.video;
}
window['ContentTheoryPlayer'] = ContentTheoryPlayer;