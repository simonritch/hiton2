<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('category.index',compact("categories"));
    }

    public function doCreate(Request $request)
    {
         $this->validate($request, [
          'label' =>  'required|unique:categories',
        ]);


      $data = $request->except("_token");

      //dd($data);
      $Campaign = new Category();
      $Campaign->label = $data['label'];
    
      $Campaign->save();

      return redirect()->back()->with('message', 'The category '.$data['label'].' has been created.');
      
    }


    public function delete($id) {
          $category = Category::find($id);

          $category->delete();

          return Redirect('categories');
    }
}
