function getHotSpotObjects()  
{
    // Corresponds with the objects in the csv file
    var objectList = [null,

@foreach($structure as $element)
    	@if($element->type == "Info")
                [ new ProductDescriptionItem("{{$element->label}}", "{{$element->url}}", "{{$element->description}}", "{{$element->image}}", {{ isset($element->auto) ? $element->auto : "false" }})
    	@elseif($element->type == "Product")
                [ new HitListItem("{{$element->label}}", "{{$element->url}}", "{{$element->image}}" {{ isset($element->auto) ? ("," . $element->auto) : "" }})
		@elseif($element->type == "Collection")
                [ new HitListSummaryItem("{{$element->label}}", "{{$element->url}}", "{{$element->image}}" {{ isset($element->auto) ? ("," . $element->auto) : "" }})
        @elseif($element->type == "Subvideo")
                [ new SubvideoItem("{{$element->label}}", "{{$element->description}}", "{{$element->image}}" {{ isset($element->auto) ? ("," . $element->auto) : "" }})
    	@endif

        @if(isset($element->children))
            @foreach($element->children as $key => $children)
                @if($children->type == "Subvideo")
                    , new SubvideoItem("{{$children->label}}", "{{$children->description}}", "{{$children->image}}",1 {{ isset($children->auto) ? ("," . $children->auto) : "" }})
                @else
                    , new HitListItem("{{$children->label}}", "{{$children->url}}", "{{$children->image}}" {{ isset($children->auto) ? ("," . $children->auto) : "" }})
                @endif
            @endforeach
        @endif
                ],  
    @endforeach
            ];   
	        return [objectList];
}